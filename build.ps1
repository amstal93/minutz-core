$tag = $args[0];
$packageJson = Get-Content 'Minutz.Core/Minutz.Core.Api/appsettings.json' -raw | ConvertFrom-Json;
$packageJson.Version=$tag
$packageJson | ConvertTo-Json  | set-content 'Minutz.Core/Minutz.Core.Api/appsettings.json'