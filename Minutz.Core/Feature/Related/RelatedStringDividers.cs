namespace Minutz.Core.Feature.Related
{
    public static class RelatedStringDividers
    {
        public const string InstanceStringDivider = "&";
        public const string MeetingStringDivider = ";";
    }
}