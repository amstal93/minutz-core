using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Minutz.Core.Feature.Related {
    public static class RelatedExtensions {
        public static string ToRelatedString (this List < (string instanceId, string meetingId) > instances) {
            StringBuilder result = new StringBuilder ();
            foreach ((string instanceId, string meetingId) instance in instances) {
                result.Append ($"{instance.instanceId}{RelatedStringDividers.InstanceStringDivider}{instance.meetingId}{RelatedStringDividers.MeetingStringDivider}");
            }
            return result.ToString ();
        }

        public static List < (string instanceId, string meetingId) > SplitToList (this string input, string split, string divider) {
            if (string.IsNullOrEmpty (input)) {
                return new List < (string instanceId, string meetingId) > ();
            }
            //throw new ArgumentException ("The input string is empty, so there for cannot split.");
            if (!input.Contains (split)) {
                return new List < (string instanceId, string meetingId) > () {
                    (input, string.Empty) };
            }
            //throw new FormatException ($"The input divider does not contain the character : {split}, to allow a valid split");
            if (!input.Contains (divider)) {
                var singleRecord = input.Split (Convert.ToChar (split));
                if (!string.IsNullOrEmpty (singleRecord[0]) && !string.IsNullOrEmpty (singleRecord[0]))
                    return new List < (string instanceId, string meetingId) > () {
                        (singleRecord[0], singleRecord[1])
                    };
                throw new FormatException ($"The split character {split}, was provided by now values were given.");
            }
            var multipleResult = new List < (string instanceId, string meetingId) > ();
            var many = input.Split (Convert.ToChar (divider)).ToList ();
            foreach (string item in many) {
                if (string.IsNullOrEmpty (item)) continue;
                var singleRecord = item.Split (Convert.ToChar (split));
                if (!string.IsNullOrEmpty (singleRecord[0]) && !string.IsNullOrEmpty (singleRecord[0]))
                    multipleResult.Add ((singleRecord[0], singleRecord[1]));
            }
            return multipleResult;
        }
    }
}