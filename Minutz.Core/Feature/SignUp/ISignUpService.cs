using System.Threading.Tasks;

namespace Minutz.Core.Feature.SignUp {
    public interface ISignUpService {
        Task<MessageBase> SignUpUserAsync (string name, string instanceId, string email);
    }
}