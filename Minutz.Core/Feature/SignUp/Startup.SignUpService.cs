using Microsoft.Extensions.DependencyInjection;

namespace Minutz.Core.Feature.SignUp {
    public static class Startup_SignUpService {
        public static void ConfigureSignUpServiceServices (this IServiceCollection services) {
            services.AddTransient<ISignUpService, SignUpService> ();
        }
    }
}