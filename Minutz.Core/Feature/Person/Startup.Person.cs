using Microsoft.Extensions.DependencyInjection;
using Minutz.Core.Feature.Person.Validation;

namespace Minutz.Core.Feature.Person
{
    public static class Startup_Person
    {
        public static void ConfigurePersonServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonValidationRepository, PersonValidationRepository>();
        }
    }
}