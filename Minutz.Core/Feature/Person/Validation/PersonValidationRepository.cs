using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace Minutz.Core.Feature.Person.Validation
{
    public class PersonValidationRepository: IPersonValidationRepository
    {
        public async Task<MessageBase> ValidatePersonExistsAsync(IDbConnection connection, string schema, string email)
        {
            try
            {
                using (IDbConnection dbConnection = connection)
                {
                    dbConnection.Open();
                    var sql = $@"SELECT * FROM [app].[Person] WHERE [Email] = '{email}' 
                                ";
                    var data = (await dbConnection.QueryAsync<Person>(sql)).ToList();
                    return data.Any() 
                        ? new MessageBase { Code = 200, Condition = true, Message = "User was found."} 
                        : new MessageBase { Code = 404, Condition = false, Message = "Not found."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        } 
    }
}