using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Minutz.Core.Feature.Related;
using Minutz.Core.Feature.SignUp;

namespace Minutz.Core.Feature.UserAdmin {
    public class UserAdminRepository : IUserAdminRepository {
        private readonly IPersonInstanceValidation _personInstanceValidation;
        private readonly ISignUpService _signUpService;

        public UserAdminRepository (IPersonInstanceValidation personInstanceValidation, ISignUpService signUpService) {
            _personInstanceValidation = personInstanceValidation;
            _signUpService = signUpService;
        }

        public async Task<UserAdminMessage> GetUsersAsync (IDbConnection connection, string schema) {
            try {
                using (IDbConnection dbConnection = connection) {
                    dbConnection.Open ();
                    var sql = $@"SELECT A.Id, P.FirstName, P.LastName, P.FullName, P.ProfilePicture, P.Email, A.Role, A.[Status]
                                FROM [{schema}].[AvailableAttendee] A
                                INNER JOIN [app].Person P
                                ON A.Email = P.Email
                                ";
                    var data = (await dbConnection.QueryAsync<InstanceUser> (sql)).ToList ();
                    return new UserAdminMessage { Code = 200, Condition = true, Message = "Success", Users = data };
                }
            }
            catch (Exception e) {
                Console.WriteLine (e);
                return new UserAdminMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public async Task<UserAdminMessage> GetAvailableUsersAsync (IDbConnection connection, string schema) {
            try {
                using (IDbConnection dbConnection = connection) {
                    dbConnection.Open ();
                    var sql = $@"SELECT A.Id, P.FirstName, P.LastName, P.FullName, P.ProfilePicture, P.Email, A.Role, A.[Status]
                                FROM [{schema}].[AvailableAttendee] A
                                INNER JOIN [app].Person P
                                ON A.Email = P.Email';
                                ";
                    var data = (await dbConnection.QueryAsync<InstanceUser> (sql)).ToList ();
                    return new UserAdminMessage { Code = 200, Condition = true, Message = "Success", Users = data };
                }
            }
            catch (Exception e) {
                Console.WriteLine (e);
                return new UserAdminMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public async Task<UserAdminMessage> UpdateUserAsync (IDbConnection connection, InstanceUser user, string schema) {
            if (string.IsNullOrEmpty (schema))
                throw new ArgumentException ("Please provide a valid schema or connection string.");
            try {
                using (IDbConnection dbConnection = connection) {
                    dbConnection.Open ();
                    var sql = $@"
                            UPDATE [{schema}].[AvailableAttendee] 
                            SET Role = '{user.Role}' , Status = '{user.Status}'
                            WHERE Id = '{user.Id.ToString()}'
                                ";
                    var data = await dbConnection.ExecuteAsync (sql);
                    return data == 1 ?
                        new UserAdminMessage { Code = 200, Condition = true, Message = "Update was successful.", User = user } :
                        new UserAdminMessage { Code = 404, Condition = false, Message = "Could not update user." };
                }
            }
            catch (Exception e) {
                Console.WriteLine (e);
                return new UserAdminMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public async Task<UserAdminMessage> AddUserAsync (IDbConnection masterConnection, InstanceUser user, string schema) {
            if (string.IsNullOrEmpty (schema))
                throw new ArgumentException ("Please provide a valid schema or connection string.");
            try {
                using (IDbConnection dbConnection = masterConnection) {
                    var getPersonSql = $@"SELECT *
                                FROM [app].Person P
                                WHERE [Email] = '{user.Email}';
                                ";
                    dbConnection.Open ();
                    var personData = (await dbConnection.QueryAsync<Person.Person> (getPersonSql)).FirstOrDefault ();

                    if (personData == null) {
                        var signUp = await _signUpService.SignUpUserAsync (user.FullName, schema, user.Email);
                        personData = (await dbConnection.QueryAsync<Person.Person> (getPersonSql)).FirstOrDefault ();
                    }
                    if (string.IsNullOrEmpty (personData.Related)) {
                        personData.Related = string.Empty;
                    }
                    var related = personData.Related.SplitToList (
                        RelatedStringDividers.InstanceStringDivider,
                        RelatedStringDividers.MeetingStringDivider);
                    if (!_personInstanceValidation.InstanceExists (related, schema)) {
                        related.Add ((schema, string.Empty));
                    }

                    var relatedString = related.ToRelatedString ();

                    var updatePersonSql = $@"
                            UPDATE [app].[Person]
                            SET Related = '{relatedString}' 
                            WHERE [Email] = '{user.Email}'
                                ";
                    var updatePersonData = await dbConnection.ExecuteAsync (updatePersonSql);
                    if (updatePersonData != 1) {
                        return new UserAdminMessage { Code = 404, Condition = false, Message = "Could not update Person." };
                    }

                    var availableAttendeeId = Guid.NewGuid ();
                    user.Id = availableAttendeeId;
                    var insertAvailableAttendeeSql = $@"
                                INSERT INTO 
                                [{schema}].AvailableAttendee
                                ([Id], [ReferenceId], [PersonIdentity], [Email], [Status], [Role]) 
                                VALUES 
                                ('{availableAttendeeId}', '{availableAttendeeId}', '{user.Email}', '{user.Email}', '{user.Status}', '{user.Role}')
                                ";
                    var data = await dbConnection.ExecuteAsync (insertAvailableAttendeeSql);
                    return data == 1 ?
                        new UserAdminMessage { Code = 200, Condition = true, Message = "Update was successful.", User = user } :
                        new UserAdminMessage { Code = 404, Condition = false, Message = "Could not insert available attendee." };
                }
            }
            catch (Exception e) {
                Console.WriteLine (e);
                return new UserAdminMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public async Task<UserAdminMessage> DeleteUserAsync (IDbConnection masterConnection, InstanceUser user, string schema) {
            if (string.IsNullOrEmpty (schema))
                throw new ArgumentException ("Please provide a valid schema or connection string.");
            try {
                using (IDbConnection dbConnection = masterConnection) {
                    var updatePersonSql = $@"
                            DELETE [{schema}].[AvailableAttendee]
                            WHERE [Id] = '{user.Id}'
                                ";
                    var updatePersonData = await dbConnection.ExecuteAsync (updatePersonSql);
                    if (updatePersonData != 1) {
                        return new UserAdminMessage { Code = 404, Condition = false, Message = "Could not update Person.", User = user };
                    }
                    return new UserAdminMessage { Code = 200, Condition = true, Message = "Success", User = user };
                }

            }
            catch (Exception e) {
                Console.WriteLine (e);
                return new UserAdminMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }
    }
}