using System.Collections.Generic;

namespace Minutz.Core.Feature.UserAdmin
{
    public class UserAdminMessage: MessageBase
    {
        public List<InstanceUser> Users { get; set; }
        public InstanceUser User { get; set; }
    }
}