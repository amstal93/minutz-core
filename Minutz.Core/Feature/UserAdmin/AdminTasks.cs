namespace Minutz.Core.Feature.UserAdmin {
    public enum AdminTasks {
        GetAvailbleUsers,
        GetUsers,
        UpdateUser,
        CreateUser,
        DeleteUser
    }
}