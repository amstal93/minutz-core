using Microsoft.AspNetCore.Mvc.Filters;
using Minutz.Core.Api.Controllers;

namespace Minutz.Core.Api
{
    public class AuthorizationHeaderFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.Controller is BaseController controller)
            {
                controller.AuthUser = context.HttpContext.Request.ExtractAuth(context.HttpContext.User);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
        
       
    }
}