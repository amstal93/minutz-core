using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts
{
    public interface IInstanceRepository
    {
        InstanceResponse GetByUsername(string username, string connectionString);
    }
}