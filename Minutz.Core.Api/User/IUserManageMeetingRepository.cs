using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.User
{
    public interface IUserManageMeetingRepository
    {
        MeetingMessage Update(Meeting meeting, string schema, string connectionString);
    }
}