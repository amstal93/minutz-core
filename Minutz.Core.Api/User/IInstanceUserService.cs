using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.User
{
    public interface IInstanceUserService
    {
        PersonResponse GetInstancePeople(AuthRestModel user);
        
        PersonResponse AddInstancePerson(Person person, AuthRestModel user);
    }
}