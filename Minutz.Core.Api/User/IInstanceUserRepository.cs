using Minutz.Core.Api.Models.Message;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.User
{
    public interface IInstanceUserRepository
    {
        PersonResponse GetInstancePeople(string schema, string connectionString);

        PersonResponse AddInstancePerson(Person person, string schema, string masterConnectionString ,string connectionString);

        PersonResponse UpdateInstancePerson(Person person, string schema, string connectionString);
    }
}