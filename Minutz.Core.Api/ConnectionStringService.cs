using System;
using Minutz.Core.Api.Contracts;

namespace Minutz.Core.Api
{
    public class ConnectionStringService : IConnectionStringService
    {
        private readonly IInstanceRepository _instanceRepository;
        
        public ConnectionStringService(IInstanceRepository instanceRepository)
        {
            _instanceRepository = instanceRepository;
        }

        private static string Server => Environment.GetEnvironmentVariable("SERVER_ADDRESS");
        
        private static string Catalogue => Environment.GetEnvironmentVariable("DEFAULT_CATALOGUE");
        
        private static string Username => Environment.GetEnvironmentVariable("DEFAULT_USER");

        private static string Password => Environment.GetEnvironmentVariable("DEFAULT_PASSWORD");
        
        public string ClientSecret => Environment.GetEnvironmentVariable("CLIENTSECRET");
        
        public string AuthorityDomain => Environment.GetEnvironmentVariable("DOMAIN");
        
        public string CreateConnectionString(string username, string password)
        {
            return $"Server={Server};User ID={username};pwd={password};database={Catalogue};";
        }

        public string CreateMasterConnectionString()
        {
            return $"Server={Server};User ID={Username};pwd={Password};database=master;";
        }

        public string AuthConnectionString()
        {
            return $"Server={Server}; database={Catalogue}_asp_auth;uid={Username};pwd={Password};pooling=true;MultipleActiveResultSets=true;";
        }

        public string CreateConnectionString()
        {
            return $"Server={Server};User ID={Username};pwd={Password};database={Catalogue};";
        }

        public string GetInstancePassword(string instance)
        {
            var instanceObject = _instanceRepository.GetByUsername(instance, CreateConnectionString());  
            return instanceObject.Instance.Password;
        }
    }
}