using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.Models.Extensions
{
    public static class AuthRestModelExtension
    {
        public static AuthRestModel ToModel
            (this Person person, bool isVerfied, string userId , string tokenExpires, string token)
        {
            var result  = new AuthRestModel();
            result.Sub = userId;
            result.Company = person.Company;
            result.Email = person.Email;
            result.FirstName = person.FirstName;
            result.IsVerified = isVerfied;
            result.InstanceId = person.InstanceId;
            result.LastName = person.LastName;
            result.Name = person.FullName;
            result.Nickname = person.FullName;
            result.Picture = person.ProfilePicture;
            result.Role = person.Role;
            result.TokenExpire = tokenExpires;
            result.IdToken = token;
            result.AccessToken = token;
            return result;
        }
        
        public static AuthRestModel UpdateFromInstance
            (this AuthRestModel model, Person person, Instance instance)
        {
            model.InstanceId = instance.Username;
            model.Company = instance.Company;
            model.Related = person.Related;
            model.Role = person.Role;
            model.FirstName = person.FirstName;
            model.LastName = person.LastName;
            model.Email = person.Email;
            return model;
        }

        public static AuthRestModel UpdateTokenInfo
            (this AuthRestModel model, UserResponseModel tokenResponseModel)
        {
            model.IdToken = tokenResponseModel.id_token;
            model.AccessToken = tokenResponseModel.access_token;
            model.TokenExpire = tokenResponseModel.expires_in;
            return model;
        }
    }
}