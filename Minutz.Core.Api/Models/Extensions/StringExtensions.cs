using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Models.Extensions
{
    public static class StringExtensions
    {
        public static string ToRelatedString (this List<(string instanceId, string meetingId)> instances)
        {
            StringBuilder result = new StringBuilder ();
            foreach((string instanceId, string meetingId) instance in instances)
            {
                result.Append($"{instance.instanceId}{Models.StringDividers.InstanceStringDivider}{instance.meetingId}{Models.StringDividers.MeetingStringDivider}");
            }
            return result.ToString ();
        }
        
        public static List<(string instanceId, string meetingId)> SplitToList (this string input, string split, string devider)
        {
            if (string.IsNullOrEmpty (input)) throw new ArgumentException ("The input string is empty, so there for cannot split.");
            if (!input.Contains(split))
            {
                return new List<(string instanceId, string meetingId)>(){(input,string.Empty)};
            }
            //throw new FormatException ($"The input devider does not contain the characher : {split}, to allow a valid split");
            if (!input.Contains (devider))
            {
                var singleRecord = input.Split (Convert.ToChar (split));
                if (!string.IsNullOrEmpty (singleRecord[0]) && !string.IsNullOrEmpty (singleRecord[0]))
                    return new List<(string instanceId, string meetingId)> ()
                    {
                        (singleRecord[0], singleRecord[1])
                    };
                throw new FormatException ($"The split character {split}, was provided by now values were given.");
            }
            var multipleResult = new List<(string instanceId, string meetingId)> ();
            var many = input.Split (Convert.ToChar (devider)).ToList ();
            foreach (string item in many)
            {
                if (string.IsNullOrEmpty (item)) continue;
                var singleRecord = item.Split (Convert.ToChar (split));
                if (!string.IsNullOrEmpty (singleRecord[0]) && !string.IsNullOrEmpty (singleRecord[0]))
                    multipleResult.Add ((singleRecord[0], singleRecord[1]));
            }
            return multipleResult;
        }
        
        public static string ToInstanceString(this AuthRestModel user)
        {
            if (user.Sub.Contains("|"))
            {
                return $"A_{user.Sub.Split ('|')[1]}";
            }
            return $"A_{user.Sub.Replace("-","_")}";
        }
        
        public static string NamedFromEmail(this string email)
        {
            return email.Split('@')[0].Replace(".", " ");
        }
    }
}