namespace Minutz.Core.Api.Models.SignalR
{
    public class HubMessage {
        
        public string Task {get;set;}
        
        public string Stamp { get; set; }
        
        public string InstanceId { get; set; }
        
        public string MeetingId { get; set; }
        
        public dynamic Data { get; set; }
    } 
}