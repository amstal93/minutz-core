using System.Collections.Generic;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.Models.Message
{
    public class PersonResponse : MessageBase
    {
        public Person Person { get; set; }
        public IEnumerable<Person> People { get; set; }
    }
}