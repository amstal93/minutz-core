using System.Collections.Generic;
using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.Models.Message
{
    public class DecisionMessage : MessageBase
    {
        public List<MinutzDecision> DecisionCollection { get; set; }
        public MinutzDecision Decision { get; set; }
    }
}