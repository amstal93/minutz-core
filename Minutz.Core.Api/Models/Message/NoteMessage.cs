using System.Collections.Generic;
using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.Models.Message
{
    public class NoteMessage: MessageBase
    {
        public List<MeetingNote> NoteCollection { get; set; }
        public MeetingNote Note { get; set; }
    }
}