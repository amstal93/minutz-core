using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Models.Message
{
    public class AuthRestModelResponse : MessageBase
    {
        public AuthRestModel InfoResponse { get; set; }
    }
}