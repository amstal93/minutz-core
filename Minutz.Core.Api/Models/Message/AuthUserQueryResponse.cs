using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Models.Message
{
    public class AuthUserQueryResponse: MessageBase
    {
        public UserQueryModel User { get; set; }
    }
}