using System;
using System.ComponentModel.DataAnnotations;

namespace Minutz.Core.Api.Models.ViewModel
{
    public class QuickNoteRequest
    {
        [Required]
        public Guid MeetingId { get; set; }
        
        [Required]
        public  string InstanceId { get; set; }
        
        [Required]
        public string NoteText { get; set; }
        public int Order { get; set; }
    }
}