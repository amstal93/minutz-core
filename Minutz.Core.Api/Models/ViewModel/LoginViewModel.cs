namespace Minutz.Core.Api.Models.ViewModel
{
    public class LoginViewModel
    {
        public string username { get; set; }
        public string instanceId { get; set; }
        public string password { get; set; }
    }
}