using System;
using System.ComponentModel.DataAnnotations;

namespace Minutz.Core.Api.Models.ViewModel
{
    public class QuickDecisionRequest
    {
        [Required]
        public Guid MeetingId { get; set; }
        
        public string InstanceId { get; set; }
        
        [Required]
        public string DecisionText { get; set; }
        public int Order { get; set; }
    }
}