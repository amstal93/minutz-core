namespace Minutz.Core.Api.Models.ViewModel
{
    public class MeetingObjectiveViewModel
    {
        public string id { get; set; }
        public string value { get; set; }
        
        public string instanceId { get; set; }
        
    }
}