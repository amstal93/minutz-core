using System;

namespace Minutz.Core.Api.Models.ViewModel
{
    public class DateViewModel
    {
        public string id { get; set; }
        public  DateTime meetingDate { get; set; }
        
        public string instanceId { get; set; }
    }
}