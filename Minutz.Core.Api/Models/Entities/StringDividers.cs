namespace Minutz.Core.Api.Models.Entities
{
    public static class StringDividers
    {
        public const string InstanceStringDivider = "&";
        public const string MeetingStringDivider = ";";
    }
}