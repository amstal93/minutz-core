namespace Minutz.Core.Api.Models.Entities
{
    public class UserQueryMetadata
    {
        public string name { get; set; }
        public string role { get; set; }
        public string instance { get; set; }
    }
}