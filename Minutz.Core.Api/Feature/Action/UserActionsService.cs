using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Action
{
    public class UserActionsService : IUserActionsService
    {
        private readonly IUserActionsRepository _userActionsRepository;
        private readonly IConnectionStringService _applicationSetting;

        public UserActionsService(IUserActionsRepository userActionsRepository, IConnectionStringService applicationSetting)
        {
            _userActionsRepository = userActionsRepository;
            _applicationSetting = applicationSetting;
        }
        
        public ActionMessage Actions(AuthRestModel user)
        {
            var instanceConnectionString = 
                _applicationSetting.CreateConnectionString(
               user.InstanceId, _applicationSetting.GetInstancePassword(user.InstanceId));
            return _userActionsRepository.Actions(user.Email, user.InstanceId, instanceConnectionString);
        }
    }
}