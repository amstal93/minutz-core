namespace Minutz.Core.Api.Feature.Action
{
    public interface IUserActionsRepository
    {
        ActionMessage Actions(string email, string schema, string connectionString);
    }
}