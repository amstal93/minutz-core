using System;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Action
{
    public interface IMinutzActionService
    {
        ActionMessage GetMeetingActions(Guid meetingId, AuthRestModel user, string schema);

        MessageBase UpdateActionComplete(Guid actionId, bool isComplete, AuthRestModel user, string schema);

        MessageBase UpdateActionText(Guid actionId, string text, AuthRestModel user, string schema);
        
        MessageBase UpdateActionTitle(Guid actionId, string text, AuthRestModel user, string schema);

        MessageBase UpdateActionAssignedAttendee(Guid actionId, string email, AuthRestModel user, string schema);

        MessageBase UpdateActionDueDate(Guid actionId, DateTime dueDate, AuthRestModel user, string schema);

        MessageBase UpdateActionRaisedDate(Guid actionId, DateTime dueDate, AuthRestModel user, string schema);

        MessageBase UpdateActionOrder(Guid actionId, int order, AuthRestModel user, string schema);

        ActionMessage QuickCreate(Guid meetingId, string actionText, int order, AuthRestModel user, string schema);

        MessageBase Delete(Guid actionId, AuthRestModel user, string schema);
    }
}