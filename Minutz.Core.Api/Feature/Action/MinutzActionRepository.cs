using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Action
{
    public class MinutzActionRepository: IMinutzActionRepository
    {
        public ActionMessage GetMeetingActions(Guid meetingId, string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var instanceSql = $@"SELECT * FROM [{schema}].[MinutzAction] WHERE [ReferenceId] = '{meetingId}'";
                    var instanceData = dbConnection.Query<MinutzAction>(instanceSql);
                    var minutzActions = instanceData.ToList();
                    foreach (var action in minutzActions)
                    {
                        if (action.ActionText.Contains("|"))
                        {
                            var splits = action.ActionText.Split("|");
                            action.ActionTitle = splits[0];
                            action.ActionText = splits[1];
                        }
                        else
                        {
                            action.ActionTitle = action.ActionText;
                        }
                    }
                    return new ActionMessage
                        {Code = 200, Condition = true, Message = "Success", Actions = minutzActions};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new ActionMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionComplete(Guid actionId, bool isComplete, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                var value = Convert.ToInt32(isComplete);
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [IsComplete] = {value} WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = isComplete.ToString()}
                        : new MessageBase
                            {Code = 404, Condition = false, Message = "Could not update action isComplete."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionText(Guid actionId, string text, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    string textValue = string.Empty;
                    var newValue = string.Empty;
                    
                    var instanceSQL = $"SELECT * FROM [{schema}].[MinutzAction] WHERE Id = '{actionId}'";
                    var instanceData = dbConnection.Query<MinutzAction>(instanceSQL);
                    var record = instanceData.FirstOrDefault();
                    if (record != null)
                    {
                        textValue = record.ActionText;
                    }

                    if (textValue.Contains("|"))
                    {
                        var split = textValue.Split("|");
                        newValue = $"{split[0]}|{text}";
                    }
                    else
                    {
                        newValue = $"|{text}";
                    }

                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [ActionText] = '{newValue}' WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = text}
                        : new MessageBase {Code = 404, Condition = false, Message = "Could not update action text."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionTitle(Guid actionId, string text, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    string textValue = string.Empty;
                    var newValue = string.Empty;
                    
                    var instanceSQL = $"SELECT * FROM [{schema}].[MinutzAction] WHERE Id = '{actionId}'";
                    var instanceData = dbConnection.Query<MinutzAction>(instanceSQL);
                    var record = instanceData.FirstOrDefault();
                    if (record != null)
                    {
                        textValue = record.ActionText;
                    }

                    if (textValue.Contains("|"))
                    {
                        var split = textValue.Split("|");
                        newValue = $"{text}|{split[1]}";
                    }
                    else
                    {
                        newValue = $"|{text}";
                    }
                    
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [ActionText] = '{newValue}' WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = text}
                        : new MessageBase {Code = 404, Condition = false, Message = "Could not update action text."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionAssignedAttendee(Guid actionId, string personId, string schema,
            string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [PersonId] = '{personId}' WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = "Success"}
                        : new MessageBase
                            {Code = 404, Condition = false, Message = "Could not update action assigned attendee."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionDueDate(Guid actionId, DateTime dueDate, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [DueDate] = '{dueDate}' WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = dueDate.ToString()}
                        : new MessageBase
                            {Code = 404, Condition = false, Message = "Could not update action due date."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionRaisedDate(Guid actionId, DateTime raisedDate, string schema,
            string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql =
                        $"UPDATE [{schema}].[MinutzAction] SET [CreatedDate] = '{raisedDate}' WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = raisedDate.ToString()}
                        : new MessageBase
                            {Code = 404, Condition = false, Message = "Could not update action due date."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase UpdateActionOrder(Guid actionId, int order, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[MinutzAction] SET [Order] = {order} WHERE Id = '{actionId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = "Success"}
                        : new MessageBase {Code = 404, Condition = false, Message = "Could not update action order."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public ActionMessage QuickCreate(Guid meetingId, string actionText, int order, string schema,
            string connectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    var id = Guid.NewGuid();
                    var textValue = $"{actionText}| ";
                    dbConnection.Open();
                    var insertSql = $@"INSERT INTO [{schema}].[MinutzAction]
                                 ([Id],[ReferenceId],[ActionText],[Order], [CreatedDate], [IsComplete]) 
                                 VALUES('{id}','{meetingId}','{textValue}', {order},'{DateTime.UtcNow}', 0 )";
                    var insertData = dbConnection.Execute(insertSql);
                    if (insertData == 1)
                    {
                        var instanceSql = $@"SELECT * FROM [{schema}].[MinutzAction] WHERE [Id] = '{id}'";
                        var instanceData = dbConnection.Query<MinutzAction>(instanceSql).FirstOrDefault();
                        if (instanceData == null)
                            return new ActionMessage
                                {Code = 404, Condition = false, Message = "Could not find quick create action item."};
                        instanceData.ActionText = string.Empty;
                        instanceData.ActionTitle =  actionText;//$"{instanceData.ActionText.Substring(0, 5)}...";
                        return new ActionMessage
                            {Code = 200, Condition = true, Message = "Success", Action = instanceData};
                    }

                    return new ActionMessage
                        {Code = 404, Condition = false, Message = "Could not quick create agenda."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new ActionMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase Delete(Guid actionId, string schema, string connectionString)
        {
            if (actionId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $@"DELETE FROM [{schema}].[MinutzAction] WHERE [Id] = '{actionId}' ";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = actionId.ToString()}
                        : new MessageBase {Code = 404, Condition = false, Message = "Could not delete action."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}