using System;

namespace Minutz.Core.Api.Feature.Action
{
    public class MinutzAction
    {
        public Guid Id { get; set; }
        public Guid ReferenceId { get; set; }
        public string ActionTitle { get; set; }
        public string ActionText { get; set; }
        public Guid PersonId { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsComplete { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public int Identifier { get; set; }
    }
}