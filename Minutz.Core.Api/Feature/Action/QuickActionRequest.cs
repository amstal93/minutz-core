using System;
using System.ComponentModel.DataAnnotations;

namespace Minutz.Core.Api.Feature.Action
{
    public class QuickActionRequest
    {
        [Required]
        public Guid MeetingId { get; set; }

        [Required]
        public string ActionText { get; set; }
        
        public string InstanceId { get; set; }

        public int Order { get; set; }
    }
}