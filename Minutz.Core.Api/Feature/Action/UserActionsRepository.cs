using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace Minutz.Core.Api.Feature.Action
{
    public class UserActionsRepository: IUserActionsRepository
    {
        public ActionMessage Actions(string email, string schema, string connectionString)
        {
            if (string.IsNullOrEmpty(email) ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $@"SELECT
                                    A.Id,
                                    A.ReferenceId,
                                    PersonIdentity,
                                    Email,
                                    Status,
                                    Role,
                                    MA.Id,
                                    MA.ReferenceId,
                                    ActionText,
                                    PersonId,
                                    CreatedDate,
                                    DueDate,
                                    IsComplete
                                FROM [{schema}].[AvailableAttendee] A
                                INNER JOIN [{schema}].MeetingAction MA
                                ON A.Id = MA.PersonId WHERE A.Email = '{email}';
                                ";
                    var data = dbConnection.Query<MinutzAction>(sql);
                    return new ActionMessage { Code = 200, Condition = true, Message = "Success", Actions = data};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new ActionMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}