using System;

namespace Minutz.Core.Api.Feature
{
    public enum TaskTypes
    {
        setMode,
        getmeetings,
        getmeeting,
/// ///////////////////////////////
        getActionCollection,
/// //////////////////////////////////////////////////////////////////
        getMeetingCollectionAgenda,
        getMeetingCollectionActions,
        getMeetingCollectionAttachments,
        getMeetingCollectionNote,
        getMeetingCollectionDecision,
        getMeetingAvailableCollection,
        getMeetingAttendeeCollection,
/// //////////////////////////////////////////////////////////////////
        createMeeting,        
        meetingNameChange,
        meetingDateChange,
        meetingLocationChange,
        meetingObjectiveChange,
        meetingOutcomeChange,
        meetingTagChange,
        meetingTimeChange,
        meetingDurationChange,
        ////////////////////// ATTENDEES
        addMeetingAttendee,
        addMeetingAttendeeInvite,
        deleteMeetingAttendee,
        updateMeetingAttendee,
        ////////////////////// ACTION
        addMeetingAction,
        assignAttendeeAction,
        assignDueAction,
        assignCreatedAction,
        assignTextAction,
        assignTitleAction,
        assignStatusAction,
        deleteMeetingAction,
        ///////////////////// NOTE
        assignNoteUpdate,
        addMeetingNote,
        deleteMeetingNote,
        //////////////////// AGENDA
        assignOrderAgenda,
        assignDurationAgenda,
        assignTitleAgenda,
        assignTextAgenda,
        assignAttendeeAgenda,
        addMeetingAgenda,
        deleteMeetingAgenda,
        ///////////////////// DECISION
        assignDecisionUpdate,
        deleteMeetingDecision,
        addMeetingDecision,
        ///////////////////// PERSON
        updatePerson
        /// //////////////////////////////////////////////////////////////////
        
    }



    public static class TaskTypesExtensions
    {
        public static TaskTypes ToTaskType(this string value)
        {
            return (TaskTypes)Enum.Parse(typeof(TaskTypes), value, true);
        }
    }
}