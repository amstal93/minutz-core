using System;
using System.Collections.Generic;
using System.Linq;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;
using Minutz.Core.Feature.Agenda;

namespace Minutz.Core.Api.Feature.Agenda
{
    public class MinutzAgendaService : IMinutzAgendaService
    {
            private readonly IConnectionStringService _applicationSetting;
    private readonly IMinutzAgendaRepository _minutzAgendaRepository;

    public MinutzAgendaService(IConnectionStringService applicationSetting, IMinutzAgendaRepository minutzAgendaRepository)
    {
      _applicationSetting = applicationSetting;
      _minutzAgendaRepository = minutzAgendaRepository;
    }

    public AgendaMessage GetMeetingAgendaCollection(Guid meetingId, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString
        ( schema, _applicationSetting.GetInstancePassword(schema));
      var data = _minutzAgendaRepository.GetMeetingAgendaCollection(meetingId, schema, instanceConnectionString);
      if (data.Condition)
      {
        if (data.AgendaCollection.Any())
        {
          var updated = new List<MeetingAgenda>();
          foreach (var agenda in data.AgendaCollection)
          {
            if (agenda.Order == 0)
            {
              agenda.Order = updated.Count;
            }

            updated.Add(agenda);
          }

          data.AgendaCollection = updated;
          data.AgendaCollection = data.AgendaCollection.OrderBy(i => i.Order).ToList();
        }
      }

      return new AgendaMessage {Condition = data.Condition, Message = data.Message, AgendaCollection = data.AgendaCollection};
    }

    public MessageBase UpdateComplete(Guid agendaId, bool isComplete, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString
        ( schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateComplete(agendaId, isComplete, schema, instanceConnectionString);
    }

    public MessageBase UpdateOrder(Guid agendaId, int order, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString
        (schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateOrder(agendaId, order, schema, instanceConnectionString);
    }

    public MessageBase UpdateDuration(Guid agendaId, int duration, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString
        (schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateDuration(agendaId, duration, schema, instanceConnectionString);
    }

    public MessageBase UpdateTitle(Guid agendaId, string title, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateTitle(agendaId, title, schema, instanceConnectionString);
    }

    public MessageBase UpdateText(Guid agendaId, string text, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateText(agendaId, text, schema, instanceConnectionString);
    }

    public MessageBase UpdateAssignedAttendee(Guid agendaId, string attendeeEmail, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.UpdateAssignedAttendee(agendaId, attendeeEmail, schema, instanceConnectionString);
    }

    public AgendaMessage QuickCreate(string meetingId, string agendaTitle, int order, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.QuickCreate(meetingId, agendaTitle, order, schema, instanceConnectionString);
    }

    public MessageBase Delete(Guid agendaId, AuthRestModel user, string schema)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
      return _minutzAgendaRepository.Delete(agendaId, schema, instanceConnectionString);
    }
    }
}