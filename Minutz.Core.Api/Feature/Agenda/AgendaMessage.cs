using System.Collections.Generic;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Agenda
{
    public class AgendaMessage: MessageBase
    {
        public List<MeetingAgenda> AgendaCollection { get; set; }
        public MeetingAgenda Agenda { get; set; }
    }
}