using System;
using System.ComponentModel.DataAnnotations;

namespace Minutz.Core.Api.Feature.Agenda
{
    public class AgendaUpdateRequest
    {
        [Required]
        public Guid Id { get; set; }
        public dynamic Value { get; set; } 
    }
}