using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public interface IGetMeetingService
    {
        MeetingMessage GetMeeting(string instanceId, Guid meetingId);
    }
}