using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Recurrence;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Recurrence
{
    public class MinutzRecurrenceService: IMinutzRecurrenceService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IMeetingRecurrenceRepository _meetingRecurrenceRepository;

        public MinutzRecurrenceService(IConnectionStringService applicationSetting, IMeetingRecurrenceRepository meetingRecurrenceRepository)
        {
            _applicationSetting = applicationSetting;
            _meetingRecurrenceRepository = meetingRecurrenceRepository;
        }
        
        public MessageBase Update(string meetingId, int recurrence, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _meetingRecurrenceRepository.Update(meetingId, recurrence, user.InstanceId, instanceConnectionString);
        }
    }
}