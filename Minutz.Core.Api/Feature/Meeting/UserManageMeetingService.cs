using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.User;
using Swashbuckle.AspNetCore.Swagger;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class UserManageMeetingService: IUserManageMeetingService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IUserManageMeetingRepository _userManageMeetingRepository;

        public UserManageMeetingService(IConnectionStringService applicationSetting, IUserManageMeetingRepository userManageMeetingRepository)
        {
            _applicationSetting = applicationSetting;
            _userManageMeetingRepository = userManageMeetingRepository;
        }

        public MeetingMessage UpdateMeeting (Meeting meeting, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString (schema, _applicationSetting.GetInstancePassword (schema));
            return _userManageMeetingRepository.Update(meeting, schema, instanceConnectionString);
        }
    }
}