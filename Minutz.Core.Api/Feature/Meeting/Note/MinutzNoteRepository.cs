using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Note;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Note
{
   public class MinutzNoteRepository : IMinutzNoteRepository
    {
        public NoteMessage GetNoteCollection(Guid meetingId, string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||string.IsNullOrEmpty(schema) || string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"SELECT * FROM [{schema}].[MeetingNote] WHERE [ReferenceId] = '{meetingId}'";
                    var instanceData = dbConnection.Query<MeetingNote> (sql).ToList();
                    foreach (MeetingNote meetingNote in instanceData)
                    {
                        if (!meetingNote.NoteText.Contains("|")) continue;
                        var noteTextValue = meetingNote.NoteText;
                        meetingNote.NoteHeader = noteTextValue.Split("|")[0];
                        meetingNote.NoteText = noteTextValue.Split("|")[1];
                    }
                    return new NoteMessage
                           {
                               Code = 200,
                               Condition = true,
                               Message = "Success",
                               NoteCollection = instanceData
                           };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NoteMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
        
        public NoteMessage QuickCreateNote(Guid meetingId, string noteText, int order, string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||string.IsNullOrEmpty(schema) || string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            var noteTextValue = $"{noteText}|{noteText}";
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var id = Guid.NewGuid();
                    var insertSql =
                        $@"INSERT INTO [{schema}].[MeetingNote]
                          (Id, ReferenceId, NoteText, CreatedDate, [Order]) 
                          VALUES('{id}','{meetingId}','{noteTextValue}| ','{DateTime.UtcNow}',{order})";
                    var insertData = dbConnection.Execute(insertSql);
                    if (insertData == 1)
                    {
                        var instanceSql = $@"SELECT * FROM [{schema}].[MeetingNote] WHERE [Id] = '{id}'";
                        var instanceData = dbConnection.Query<MeetingNote>(instanceSql).FirstOrDefault();
                        
                        if (instanceData == null)
                            return new NoteMessage
                                   {
                                       Code = 404,
                                       Condition = false,
                                       Message = "Could not find quick create decision item."
                                   };
                        instanceData.NoteHeader = noteText;
                        instanceData.NoteText = string.Empty;
                        return new NoteMessage
                               {
                                   Code = 200,
                                   Condition = true,
                                   Message = "Success",
                                   Note = instanceData
                               };
                    }
                    return new NoteMessage
                           {
                               Code = 404,
                               Condition = false,
                               Message = "Could not quick create decision."
                           };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NoteMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
        
        public NoteMessage UpdateNote(Guid meetingId, MeetingNote note, string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||string.IsNullOrEmpty(schema) || string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            var noteTextValue = $"{note.NoteHeader}|{note.NoteText}";
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $@"UPDATE [{schema}].[MeetingNote] SET
                                   [CreatedDate] = '{note.CreatedDate}',
                                   [NoteText] = '{noteTextValue}',
                                   [Order] = {note.Order}
                                 WHERE Id = '{note.Id}'";
                    if (!string.IsNullOrEmpty(note.MeetingAttendeeId))
                    {
                        sql = $@"UPDATE [{schema}].[MeetingNote] SET
                                   [CreatedDate] = '{note.CreatedDate}',
                                   [NoteText] = '{noteTextValue}',
                                   [MeetingAttendeeId] = '{note.MeetingAttendeeId}',
                                   [Order] = {note.Order}
                                 WHERE Id = '{note.Id}'";
                    }

                    var data = dbConnection.Execute(sql);
                    
                    return data == 1 
                        ? new NoteMessage{ Code = 200, Condition =  true, Message = "Success", Note = note} 
                        : new NoteMessage{ Code = 404, Condition =  false, Message = "Could not update meeting."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NoteMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
        
        public MessageBase DeleteNote(Guid noteId, string schema, string connectionString)
        {
            if (noteId == Guid.Empty  ||string.IsNullOrEmpty(schema) || string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"DELETE FROM [{schema}].[MeetingNote] WHERE Id = '{noteId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1 
                        ? new MessageBase{ Code = 200, Condition =  true, Message = noteId.ToString()} 
                        : new MessageBase{ Code = 404, Condition =  false, Message = "Could not update meeting."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}