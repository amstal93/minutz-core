using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.User;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class UserManageMeetingRepository : IUserManageMeetingRepository
    {
        public MeetingMessage Update(Meeting meeting, string schema, string connectionString)
        {
            if (meeting.Id == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    var isRecurrence = Convert.ToInt32(meeting.IsRecurrence);
                    var isPrivate = Convert.ToInt32(meeting.IsPrivate);
                    var isLocked = Convert.ToInt32(meeting.IsLocked);
                    var isFormal = Convert.ToInt32(meeting.IsFormal);
                    dbConnection.Open();
                    var sql = $@"UPDATE [{schema}].[Meeting] 
                                    SET [Name] = '{meeting.Name}',
                                     [Location] = '{meeting.Location}',
                                     [Date] = '{meeting.Date}',
                                     [UpdatedDate] = '{DateTime.UtcNow}',
                                     [Time] = '{meeting.Time}',
                                     [Duration] = {meeting.Duration},
                                     [IsRecurrence] = {isRecurrence},
                                     [IsPrivate] = {isPrivate},
                                     [RecurrenceType] = '{meeting.RecurrenceType}',
                                     [IsLocked] = {isLocked},
                                     [IsFormal] = {isFormal},
                                     [TimeZone] = '{meeting.TimeZone}',
                                     [Tag] = '{meeting.Tag}',
                                     [Purpose] = '{meeting.Purpose}',
                                     [Status] = '{meeting.Status}',
                                     [MeetingOwnerId] = '{meeting.MeetingOwnerId}',
                                     [Outcome] = '{meeting.Outcome}'
                                   WHERE Id = '{meeting.Id}'";
                    var data = dbConnection.Execute(sql);//todo: add  [RecurrenceData] = '{meeting.RecurrenceType}',
                    return data == 1
                        ? new MeetingMessage {Code = 200, Condition = true, Message = "Success", Meeting = meeting}
                        : new MeetingMessage
                          {
                              Code = 404,
                              Condition = false,
                              Message = "Could not update meeting values."
                          };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MeetingMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}