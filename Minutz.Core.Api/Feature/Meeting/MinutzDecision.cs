using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class MinutzDecision
    {
        public Guid Id { get; set; }
        
        public Guid ReferenceId { get; set; }
        
        public string InstanceId { get; set; }
        public string DecisionHeader { get; set; }
        public string DecisionText { get; set; }
        public string DecisionComment { get; set; }
        public string AgendaId { get; set; }
        public string PersonId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsOverturned { get; set; }
        public int Order { get; set; }
        public int Identifier { get; set; }
    }
}