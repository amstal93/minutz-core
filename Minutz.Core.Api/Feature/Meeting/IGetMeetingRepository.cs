using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public interface IGetMeetingRepository
    {
        MeetingMessage Get(Guid meetingId, string schema, string connectionString);
    }
}