using System.Collections.Generic;
using Minutz.Core.Api.Models.Message;


namespace Minutz.Core.Api.Feature.Meeting
{
    public class MeetingMessage : MessageBase
    {
        public Meeting Meeting { get; set; }
        public List<Meeting> Meetings { get; set; }
    }
}