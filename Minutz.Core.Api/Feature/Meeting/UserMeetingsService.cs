using System;
using System.Collections.Generic;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Extensions;
using Minutz.Core.Api.User;

namespace Minutz.Core.Feature.Meeting
{
    public class UserMeetingsService : IUserMeetingsService
    {
            private readonly IUserMeetingsRepository _userMeetingsRepository;
    private readonly IConnectionStringService _applicationSetting;
    private readonly IUserRepository _userRepository;

    public UserMeetingsService(
      IUserMeetingsRepository userMeetingsRepository,
      IConnectionStringService applicationSetting,
      IUserRepository userRepository)
    {
      _userMeetingsRepository = userMeetingsRepository;
      _applicationSetting = applicationSetting;
      _userRepository = userRepository;
    }

    public MeetingMessage CreateEmptyUserMeeting(AuthRestModel user)
    {
      var instanceConnectionString = _applicationSetting.CreateConnectionString(user.InstanceId, _applicationSetting.GetInstancePassword(user.InstanceId));
      return _userMeetingsRepository.CreateEmptyUserMeeting(user.Email, user.InstanceId, instanceConnectionString);
    }

    public MeetingMessage Meetings(AuthRestModel user)
    {
      var result = new MeetingMessage {Code = 500, Condition = false, Meetings = new List<Api.Feature.Meeting.Meeting>()};

      var instanceConnectionString = _applicationSetting.CreateConnectionString(user.InstanceId, _applicationSetting.GetInstancePassword(user.InstanceId));
      var masterConnectionString = _applicationSetting.CreateConnectionString();

      var userMeetings = _userMeetingsRepository.Meetings(user.Email, user.InstanceId, instanceConnectionString);
      result.Code = userMeetings.Code;
      result.Message = userMeetings.Message;
      result.Condition = userMeetings.Condition;
      result.Meetings.AddRange(userMeetings.Meetings);
      var dbUser = _userRepository.GetUserByEmail(user.Email, "app", masterConnectionString);
      if (dbUser == null) return result;

      try
      {
        if (!string.IsNullOrEmpty(dbUser.Related))
        {
          foreach (var relatedItem in dbUser.RelatedItems())
          {
            var relatedConnectionStringPassword = _applicationSetting.GetInstancePassword(relatedItem.instanceId);
            var relatedConnectionString = _applicationSetting.CreateConnectionString(relatedItem.instanceId, relatedConnectionStringPassword);
            var dbUserMeetings = _userMeetingsRepository.AttendeeMeetings(user.Email, relatedItem.instanceId, relatedConnectionString);
            if (dbUserMeetings.Condition)
            {
              result.Meetings.AddRange(dbUserMeetings.Meetings);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
      return result;
    }

    public MeetingMessage Meeting(AuthRestModel user, Guid meetingId, string instanceId)
    {
      var schema = user.InstanceId;
      if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
      var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
      return _userMeetingsRepository.Meeting(meetingId, schema, instanceConnectionString);
    }
    }
}