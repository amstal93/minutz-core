using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Title;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Title
{
    public class MinutzTitleService : IMinutzTitleService
    {
        private readonly IMinutzTitleRepository _minutzTitleRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzTitleService(IMinutzTitleRepository minutzTitleRepository, IConnectionStringService applicationSetting)
        {
            _minutzTitleRepository = minutzTitleRepository;
            _applicationSetting = applicationSetting;
        }

        public MessageBase Update(string meetingId, string title, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzTitleRepository.Update(meetingId, title, user.InstanceId, instanceConnectionString);
        }
    }
}