using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Tag;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Tag
{
    public class MinutzTagRepository: IMinutzTagRepository
    {
        public MessageBase Update(string meetingId, string tags, string schema, string connectionString)
        {
            if ( string.IsNullOrEmpty(meetingId) ||string.IsNullOrEmpty(schema) || string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[Meeting] SET Tag ='{tags}' WHERE Id = '{meetingId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1 
                        ? new MessageBase{ Code = 200, Condition =  true, Message = "Success"} 
                        : new MessageBase{ Code = 404, Condition =  false, Message = "Could not update meeting."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}