using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class MeetingNote
    {
        public Guid Id { get; set; }
        
        public Guid ReferenceId { get; set; }
        public string NoteText { get; set; }

        public string NoteHeader { get; set; }
        
        public string InstanceId { get; set; }
        public string MeetingAttendeeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Order { get; set; }
        public int Identifier { get; set; }
    }
}