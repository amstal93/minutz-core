using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.ObjectivePurpose;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Meeting.ObjectivePurpose
{
    public class MeetingObjectivePurposeService: IMeetingObjectivePurposeService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IMeetingObjectivePurposeRepository _meetingObjectivePurposeRepository;

        public MeetingObjectivePurposeService(IConnectionStringService applicationSetting,
            IMeetingObjectivePurposeRepository meetingObjectivePurposeRepository)
        {
            _applicationSetting = applicationSetting;
            _meetingObjectivePurposeRepository = meetingObjectivePurposeRepository;
        }

        public MeetingMessage UpdateOutcome(Guid meetingId, string objective, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString ( schema, _applicationSetting.GetInstancePassword (schema));
            return _meetingObjectivePurposeRepository.UpdateOutcome(meetingId, objective, schema,
                instanceConnectionString);
        }

        public MeetingMessage UpdatePurpose(Guid meetingId, string purpose, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString (schema, _applicationSetting.GetInstancePassword (schema));
            return _meetingObjectivePurposeRepository.UpdatePurpose(meetingId, purpose, schema,
                instanceConnectionString);
        }
    }
}