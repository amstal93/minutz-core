using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Status;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Status
{
    public class MeetingStatusService : IMeetingStatusService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IMeetingStatusRepository _meetingStatusRepository;

        public MeetingStatusService(IConnectionStringService applicationSetting, IMeetingStatusRepository meetingStatusRepository)
        {
            _applicationSetting = applicationSetting;
            _meetingStatusRepository = meetingStatusRepository;
        }

        public MessageBase UpdateMeetingStatus(Guid meetingId, string status, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _meetingStatusRepository.UpdateMeetingStatus(meetingId, status, schema,instanceConnectionString);
        }
    }
}