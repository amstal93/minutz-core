using System;
using System.Collections.Generic;
using Minutz.Core.Api.Feature.Action;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class MeetingViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Time { get; set; }
        public int Duration { get; set; }
        public bool IsRecurrence { get; set; }
        public bool IsPrivate { get; set; }
        public int RecurrenceType { get; set; }
        public bool IsLocked { get; set; }
        public bool IsFormal { get; set; }
        public string Status { get; set; }
        public string Location {get;set;}
        public string TimeZone { get; set; }
        public int TimeZoneOffSet{ get; set;}
        public List<string> Tag { get; set; }
        public string Purpose { get; set; }
        public string MeetingOwnerId { get; set; }
        public string Outcome { get; set; }
    
        public List<MeetingAgenda> Agenda { get; set; }
    
        public List<MeetingAgenda> MeetingAgendaCollection { get; set; }
        public List<MeetingAttachment> MeetingAttachmentCollection { get; set; }
        public List<Models.Entities.MeetingAttendee> MeetingAttendeeCollection { get; set; }
        public List<Models.Entities.MeetingAttendee> AvailableAttendeeCollection { get; set; }
        public List<MeetingNote> MeetingNoteCollection { get; set; }
        public List<MinutzAction> MeetingActionCollection { get; set; }
        public List<MinutzDecision> MeetingDecisionCollection { get; set; }

        public string ResultMessage { get; set; }
    }
}