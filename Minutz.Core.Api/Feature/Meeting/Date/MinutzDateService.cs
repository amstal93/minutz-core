using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Date;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Date
{
    public class MinutzDateService : IMinutzDateService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IMinutzDateRepository _minutzDateRepository;

        public MinutzDateService(IConnectionStringService applicationSetting, IMinutzDateRepository minutzDateRepository)
        {
            _applicationSetting = applicationSetting;
            _minutzDateRepository = minutzDateRepository;
        }

        public MessageBase Update(string meetingId, DateTime date, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzDateRepository.Update(meetingId, date, user.InstanceId, instanceConnectionString);
        }
    }
}