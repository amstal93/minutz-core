using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Meeting
{
    public interface IUserManageMeetingService
    {
        MeetingMessage UpdateMeeting(Meeting meeting, AuthRestModel user, string schema);
    }
}