using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class UserMeetingsRepository : IUserMeetingsRepository
    {
        public MeetingMessage Meetings (string email, string schema, string connectionString) 
        {
            if (string.IsNullOrEmpty (email) ||
                string.IsNullOrEmpty (schema) ||
                string.IsNullOrEmpty (connectionString))
                throw new ArgumentException ("Please provide a valid meeting identifier, schema or connection string.");
            try {
                using (IDbConnection dbConnection = new SqlConnection (connectionString)) {
                    dbConnection.Open ();
                    var sql = $"SELECT * FROM [{schema}].[Meeting] WHERE MeetingOwnerId = '{email}'";
                    var data = dbConnection.Query<Meeting> (sql);
                    var resultData = new List<Meeting>();
                    foreach (var meeting in data.ToList())
                    {
                        meeting.InstanceId = schema;
                        resultData.Add(meeting);
                    }
                    return new MeetingMessage { Code = 200, Condition = true, Message = "Success", Meetings = resultData };
                }
            } catch (Exception e) {
                Console.WriteLine (e);
                return new MeetingMessage { Code = 500, Condition = false, Message = e.Message, Meetings = new List<Meeting>()};
            }
        }
        
        public MeetingMessage AttendeeMeetings (string email, string schema, string connectionString) 
        {
            if (string.IsNullOrEmpty (email) ||
                string.IsNullOrEmpty (schema) ||
                string.IsNullOrEmpty (connectionString))
                throw new ArgumentException ("Please provide a valid meeting identifier, schema or connection string.");
            try {
                using (IDbConnection dbConnection = new SqlConnection (connectionString)) {
                    dbConnection.Open ();
                    var sql = $"SELECT * FROM [{schema}].[MeetingAttendee] WHERE [Email] = '{email}'";
                    var data = dbConnection.Query<Models.Entities.MeetingAttendee> (sql);
                    var meetingMessage = new MeetingMessage {Code = 200, Condition = true, Message = "Success", Meetings = new List<Meeting>()};
                    foreach (var attendee in data)
                    {
                        attendee.ReferenceId = attendee.ReferenceId;
                        var meeting = Meeting(attendee.ReferenceId, schema, connectionString);
                        if (meeting.Condition)
                        {
                            meeting.Meeting.InstanceId = schema;
                            meetingMessage.Meetings.Add(meeting.Meeting);
                        }
                    }

                    return meetingMessage;
                }
            } catch (Exception e) {
                Console.WriteLine (e);
                return new MeetingMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public MeetingMessage CreateEmptyUserMeeting (string email, string schema, string connectionString) {
            if (string.IsNullOrEmpty (schema) ||
                string.IsNullOrEmpty (connectionString))
                throw new ArgumentException ("Please provide a valid meeting identifier, schema or connection string.");
            try {
                using (IDbConnection dbConnection = new SqlConnection (connectionString)) {
                    dbConnection.Open ();
                    var Id = Guid.NewGuid ();
                    var insertSql = $@"INSERT INTO [{schema}].[Meeting]
                                    ([Id], [Name], [Location], [Date],[UpdatedDate],[Time],[Duration],[IsRecurrence],[IsPrivate],[RecurrenceType],[IsLocked],[IsFormal],[MeetingOwnerId],[Status])
                                    VALUES ('{Id}', 'Minutz','', GETDATE(), GETDATE(),'0:00', 0, 0, 0, 0, 0, 0, '{email}','create');";
                    var instanceSql = $"SELECT * FROM [{schema}].[Meeting] WHERE [Id] = '{Id}'";
                    var insertData = dbConnection.Execute (insertSql);
                    if (insertData == 1) {
                        var insertAttendeeSql = $@"INSERT INTO [{schema}].[MeetingAttendee]
                                                ([Id],[ReferenceId], [PersonIdentity], [Email], [Status], [Role])
                                                VALUES(NEWID(), NEWID() , '{email}', '{email}', 'Accepted', 'Owner');";
                        var insertAttendeeData = dbConnection.Execute (insertAttendeeSql);
                        var data = dbConnection.Query<Meeting> (instanceSql).FirstOrDefault ();
                        data.InstanceId = schema;
                        return new MeetingMessage { Code = 200, Condition = true, Message = "Success", Meeting = data };
                    }
                    return new MeetingMessage { Code = 404, Condition = false, Message = "Could not quick create meeting." };
                }
            } catch (Exception e) {
                Console.WriteLine (e);
                return new MeetingMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }

        public MeetingMessage Meeting (Guid meetingId, string schema, string connectionString) {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty (schema) ||
                string.IsNullOrEmpty (connectionString))
                throw new ArgumentException ("Please provide a valid meeting identifier, schema or connection string.");
            try {
                using (IDbConnection dbConnection = new SqlConnection (connectionString)) {
                    dbConnection.Open ();
                    var instanceSql = $"SELECT * FROM [{schema}].[Meeting] WHERE [Id] = '{meetingId}'";
                    var data = dbConnection.Query<Meeting> (instanceSql).FirstOrDefault ();
                    data.InstanceId = schema;
                    return new MeetingMessage { Code = 200, Condition = true, Message = "Success", Meeting = data };
                }
            } catch (Exception e) {
                Console.WriteLine (e);
                return new MeetingMessage { Code = 500, Condition = false, Message = e.Message };
            }
        }
    }
}