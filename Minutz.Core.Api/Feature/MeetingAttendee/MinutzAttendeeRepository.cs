using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Dapper;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public class MinutzAttendeeRepository : IMinutzAttendeeRepository
    {
        /// <summary>
        /// Get the attendees for a meeting
        /// </summary>
        /// <param name="meetingId">Current meeting Id</param>
        /// <param name="schema">Current instance id</param>
        /// <param name="connectionString">Instance connection string</param>
        /// <param name="masterConnectionString"></param>
        /// <returns>Collection of meeting attendees</returns>
        /// <exception cref="ArgumentException"></exception>
        public AttendeeMessage GetAttendees(Guid meetingId, string schema, string connectionString, string masterConnectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                List<Person> people;
                using (IDbConnection dbConnection = new SqlConnection(masterConnectionString))
                {
                    var peopleSql = $@"SELECT * FROM [app].[Person]";
                    var peopleData = dbConnection.Query<Person>(peopleSql).ToList();
                    people = peopleData;
                }


                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var meetingSql = $@"SELECT * FROM [{schema}].[Meeting] WHERE [Id] = '{meetingId.ToString()}'";
                    var meetingInstance = dbConnection.Query<Meeting.Meeting> (meetingSql).FirstOrDefault();
                    var meetingOwnerInstance = people.FirstOrDefault(i => string.Equals(i.Email, meetingInstance.MeetingOwnerId, StringComparison.OrdinalIgnoreCase));
                    var meetingOwner = new Models.Entities.MeetingAttendee
                                       {
                                           PersonIdentity = meetingOwnerInstance.Email,
                                           Id = Guid.NewGuid(),
                                           Role = "Owner",
                                           Status = "Accepted",
                                           Email = meetingOwnerInstance.Email,
                                           Name = string.IsNullOrEmpty(meetingOwnerInstance.FullName)
                                               ? $"{meetingOwnerInstance.FirstName} {meetingOwnerInstance.LastName}"
                                               : meetingOwnerInstance.FullName,
                                           Picture = meetingOwnerInstance.ProfilePicture
                                       };
                    var ownerSql = $@"SELECT Id FROM 
                                      [{schema}].[MeetingAttendee] 
                                      WHERE Email = '{meetingOwnerInstance.Email}' 
                                      AND [ReferenceId]='{meetingId}' ";
                    var ownerData = dbConnection.Query<Models.Entities.MeetingAttendee> (ownerSql).ToList();
                    if (!ownerData.Any())
                    {
                        var insertSql = $@"INSERT INTO [{schema}].[MeetingAttendee]
                                        ([Id],[ReferenceId],[PersonIdentity],[Email],[Role],[Status]) 
                                        VALUES
                                        ('{meetingOwner.Id}','{meetingId}','{meetingOwner.PersonIdentity}','{meetingOwner.Email}','{meetingOwner.Role}','{meetingOwner.Status}')
                                        ";
                        var data = dbConnection.Execute(insertSql);
                    }

                    var instanceSql = $@"SELECT * FROM [{schema}].[MeetingAttendee] WHERE [ReferenceId] = '{meetingId}'";
                    var instanceData = dbConnection.Query<Models.Entities.MeetingAttendee> (instanceSql).ToList();
                    
                    
                    var attendees = new List<Models.Entities.MeetingAttendee>();
                    foreach (var attendee in instanceData)
                    {
                        var att = new Models.Entities.MeetingAttendee
                                  {
                                      PersonIdentity = attendee.PersonIdentity,
                                      Id = attendee.Id,
                                      Role = attendee.Role,
                                      Status = attendee.Status,
                                      Email = attendee.Email,
                                      Company = attendee.Company
                                  };
                        var person = people.FirstOrDefault(i => i.IdentityId == attendee.PersonIdentity);
                        if (person != null)
                        {
                            att.Name = string.IsNullOrEmpty(person.FullName)
                                ? $"{person.FirstName} {person.LastName}"
                                : person.FullName;
                            att.Picture = person.ProfilePicture;
                        }

                        attendees.Add(att);
                    }
                    
                    //attendees.Add(meetingOwner);
                    return new AttendeeMessage
                           {
                               Code = 200,
                               Condition = true,
                               Message = "Success",
                               Attendees = attendees
                           };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new AttendeeMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }
        
        /// <summary>
        /// Add an attendee into the database
        /// </summary>
        /// <param name="meetingId"></param>
        /// <param name="attendee">Full attendee entity</param>
        /// <param name="schema">Current instance id</param>
        /// <param name="connectionString">Full connection string for instance</param>
        /// <returns>Attendee Message with the attendee</returns>
        /// <exception cref="ArgumentException"></exception>
        public AttendeeMessage AddAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var insertSql = $@"INSERT INTO [{schema}].[MeetingAttendee]
                                        ([Id],[ReferenceId],[PersonIdentity],[Email],[Role],[Status]) 
                                        VALUES
                                        ('{attendee.Id}','{meetingId}','{attendee.PersonIdentity}','{attendee.Email}','{attendee.Role}','{attendee.Status}')
                                        ";

                    var data = dbConnection.Execute(insertSql);
                    return data == 1
                        ? new AttendeeMessage {Code = 200, Condition = true, Message = "Success", Attendee = attendee}
                        : new AttendeeMessage
                          {
                              Code = 404,
                              Condition = false,
                              Message = "Could not insert attendee."
                          };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new AttendeeMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public AttendeeMessage UpdateAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, string schema, string connectionString, string masterConnectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, schema or connection string.");
            try
            {
                if (attendee.PersonIdentity == "Invited")
                {
                    using (IDbConnection dbConnection = new SqlConnection(masterConnectionString))
                    {
                        var checkInstanceSql = $@"SELECT * FROM [app].[Instance] WHERE [Name] = '{attendee.Email}'";
                        var checkInstanceData = dbConnection.Query<Models.Entities.Instance> (checkInstanceSql)
                                                            .ToList();
                        if (checkInstanceData.Any())
                        {
                            var instanceId = $"A_{checkInstanceData.FirstOrDefault().Id.ToString().Replace('-', '_')}"; 
                            attendee.PersonIdentity = attendee.Email;
                            try
                            {
                                var nameString = CultureInfo.CurrentCulture.TextInfo
                                    .ToTitleCase(attendee.Email.Split('@')[0]
                                        .Replace('.', ' '));

                                var nameStrings = nameString.Split(' ').ToList();
                                var updatePersonSql = $@"UPDATE [app].[Person]
                                        SET [FullName] = '{nameString}' ,
                                            [FirstName] = '{nameStrings.First()}' , 
                                            [LastName] = '{nameStrings.Last()}' ,
                                            [InstanceId] = '{instanceId}' ,
                                            [Company] = '{attendee.Company}'
                                        WHERE [Email] = '{attendee.Email}' 
                                        ";
                                var dataPerson = dbConnection.Execute(updatePersonSql);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                    }
                }
                
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var insertSql = $@"UPDATE [{schema}].[MeetingAttendee]
                                        SET 
                                         [Role] = '{attendee.Role}' 
                                        ,[Status] = '{attendee.Status}' 
                                        ,[PersonIdentity] = '{attendee.PersonIdentity}' 
                                        ,[Company] = '{attendee.Company}'
                                        WHERE [referenceId] = '{attendee.ReferenceId.ToString()}' 
                                        AND [Email] = '{attendee.Email}'
                                        ";

                    var data = dbConnection.Execute(insertSql);
                    return data == 1
                        ? new AttendeeMessage {Code = 200, Condition = true, Message = "Success", Attendee = attendee}
                        : new AttendeeMessage
                          {
                              Code = 404,
                              Condition = false,
                              Message = "Could not insert attendee."
                          };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new AttendeeMessage {Code = 500, Condition = false, Message = e.Message};
            }
        }

        public MessageBase DeleteAttendee(Guid meetingId,string attendeeEmail ,string schema, string connectionString)
        {
            if (meetingId == Guid.Empty ||
                string.IsNullOrEmpty(attendeeEmail) ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid agenda identifier, attendee email, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var instanceSql = $@"DELETE FROM [{schema}].[MeetingAttendee] 
                                         WHERE [Email] = '{attendeeEmail}' 
                                         AND [ReferenceId]= '{meetingId.ToString()}' ";
                    var data = dbConnection.Execute(instanceSql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = attendeeEmail}
                        : new MessageBase
                          {
                              Code = 404,
                              Condition = false,
                              Message = "Could not remove attendee."
                          };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}