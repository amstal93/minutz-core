using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Invitation
{
    public interface IInvitationService
    {
        MessageBase InviteUser(AuthRestModel user, Api.Models.Entities.MeetingAttendee invitee, Api.Feature.Meeting.Meeting meeting);
         
        bool SendMeetingInvitation(Api.Models.Entities.MeetingAttendee attendee, MeetingViewModel meeting,
            string instanceId);
    }
}