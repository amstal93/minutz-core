namespace Minutz.Core.Api.Feature.Invitation
{
    public enum InviteAttendees
    {
        AllAttendees = 1,
        NewAttendees = 2,
        Custom = 3
    }
}