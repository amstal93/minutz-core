using System;
using System.Collections.Generic;

namespace Minutz.Core.Api.Feature.Invitation
{
    public class InviteRequestMessage
    {
        public Guid MeetingId { get; set; }
        public List<string> customRecipients { get; set; }
        public string message { get; set; }
        public bool includeAgenda { get; set; }
        public bool includeAttachments { get; set; }
    }
}