using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.MeetingAttendee;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Controllers.Feature.Meeting
{
    
    public class MinutzAttendeeController : BaseController
    {
        private readonly IMinutzAttendeeService _minutzAttendeeService;

        public MinutzAttendeeController(IMinutzAttendeeService minutzAttendeeService)
        {
            _minutzAttendeeService = minutzAttendeeService;
        }

        [Authorize]
        [HttpGet("api/feature/attendee/attendees", Name = "Get Attendees for meeting")]
        public IActionResult GetMeetingAttendeesResult(string meetingId, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
            var result = _minutzAttendeeService.GetAttendees(Guid.Parse(meetingId),AuthUser.InfoResponse , schema);
            return result.Condition ? Ok(result.Attendees) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/attendee/update", Name = "Update Attendee for meeting")]
        public IActionResult GetAvailableAttendeesResult([FromBody] MeetingAttendee attendee)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(attendee.InstanceId)) schema = attendee.InstanceId;
            var result = _minutzAttendeeService.UpdateAttendee(attendee.ReferenceId, attendee,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Attendees) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/attendee/add", Name = "Add Attendee to Meeting")]
        public IActionResult AddMeetingAttendeeResult([FromBody] MeetingAttendee attendee) 
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(attendee.InstanceId)) schema = attendee.InstanceId;
            var result = _minutzAttendeeService.AddAttendee(attendee.ReferenceId,attendee,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Attendees) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpDelete("api/feature/attendee/remove", Name = "Remove Attendee from Meeting")]
        public IActionResult GetAvailableAttendeesResult(string meetingId, string attendeeEmail, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
            var result = _minutzAttendeeService.DeleteAttendee(Guid.Parse(meetingId), attendeeEmail,AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
    }
}