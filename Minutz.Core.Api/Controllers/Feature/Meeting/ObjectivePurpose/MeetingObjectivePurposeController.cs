﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts.ObjectivePurpose;
using Minutz.Core.Api.Models.ViewModel;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.ObjectivePurpose
{
    public class MeetingObjectivePurposeController : BaseController
    {
        private readonly IMeetingObjectivePurposeService _meetingObjectivePurposeService;

        public MeetingObjectivePurposeController(IMeetingObjectivePurposeService meetingObjectivePurposeService)
        {
            _meetingObjectivePurposeService = meetingObjectivePurposeService;
        }    

        [Authorize]
        [HttpPost("api/feature/objective/update", Name = "Update meeting objective")]
        public IActionResult UpdateObjectiveMeetingUpdateRequest([FromBody] MeetingObjectiveViewModel request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.instanceId)) schema = request.instanceId;
            var result = _meetingObjectivePurposeService.UpdateOutcome
                (new Guid(request.id), request.value, AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok(result.Meeting) : StatusCode(result.Code, result.Message);
        }

        [Authorize]
        [HttpPost("api/feature/purpose/update", Name = "Update meeting purpose")]
        public IActionResult UpdatePurposeMeetingUpdateRequest([FromBody] MeetingObjectiveViewModel request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.instanceId)) schema = request.instanceId;
            var result = _meetingObjectivePurposeService.UpdatePurpose
                (new Guid(request.id), request.value, AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok(result.Meeting) : StatusCode(result.Code, result.Message);
        }
    }
}