﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Title;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingTitleController : BaseController
    {
        private readonly IMinutzTitleService _minutzTitleService;

        public MeetingTitleController(IMinutzTitleService minutzTitleService)
        {
            _minutzTitleService = minutzTitleService;
        }

        [Authorize]
        [HttpPost("api/feature/header/title", Name = "Update Meeting title")]
        public IActionResult UpdateMeetingTitleResult(string id, string title, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzTitleService.Update(id, title, AuthUser.InfoResponse, schema);
            if (result.Condition)
            {
                return Ok();
            }
            return StatusCode(result.Code, result.Message);
        }
    }
}