﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Location;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingLocationController : BaseController
    {
        private readonly IMinutzLocationService _minutzLocationService;

        public MeetingLocationController(IMinutzLocationService minutzLocationService)
        {
            _minutzLocationService = minutzLocationService;
        }

        [Authorize]
        [HttpPost("api/feature/header/location", Name = "Update Meeting location")]
        public IActionResult UpdateMeetingLocationResult(string id, string location, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzLocationService.Update(id, location, AuthUser.InfoResponse, schema);
            if (result.Condition)
            {
                return Ok();
            }
            return StatusCode(result.Code, result.Message);
        }
    }
}