using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Note;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.ViewModel;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Note
{
    
    public class MinutzNoteController : BaseController
    {
        private readonly IMinutzNoteService _noteService;

        public MinutzNoteController(IMinutzNoteService noteService)
        {
            _noteService = noteService;
        }
        
        [Authorize]
        [HttpGet("api/feature/notes", Name = "Get the notes for a meeting")]
        public IActionResult GetMeetingNotesResult(string meetingId, string instanceId)
        {
            if (string.IsNullOrEmpty(meetingId))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _noteService.GetMeetingNotes
                (Guid.Parse(meetingId) ,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.NoteCollection) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPut("api/feature/note/quick", Name = "Quick create note")]
        public IActionResult QuickCreateNoteResult([FromBody] QuickNoteRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _noteService.QuickNoteCreate
                (request.MeetingId, request.NoteText,request.Order ,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Note) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/note", Name = "Update note for a meeting")]
        public IActionResult UpdateNoteResult([FromBody] MeetingNote request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _noteService.UpdateNote
                (request.Id, request ,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Note) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpDelete("api/feature/note", Name = "Delete note for a meeting")]
        public IActionResult DeleteDecisionResult(string noteId, string meetingId, string instanceId)
        {
            if (string.IsNullOrEmpty(noteId))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _noteService.DeleteNote
                (Guid.Parse(noteId), AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
    }
}