﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Feature.Agenda;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Agenda
{
  
  public class MeetingAgendaController : BaseController
  {
    private readonly IMinutzAgendaService _minutzAgendaService;

    public MeetingAgendaController(IMinutzAgendaService minutzAgendaService)
    {
      _minutzAgendaService = minutzAgendaService;
    }

    [Authorize]
    [HttpGet("api/feature/agenda/meeting", Name = "Get meeting agenda collection")]
    public IActionResult GetMeetingAgendaCollectionResult(string meetingId, string instanceId)
    {
      if (string.IsNullOrEmpty(meetingId))
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
      var result = _minutzAgendaService.GetMeetingAgendaCollection(Guid.Parse(meetingId), AuthUser.InfoResponse, schema);
      return result.Condition ? Ok(result.AgendaCollection) : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpGet("api/feature/agenda/collection", Name = "Get the collection of agenda items")]
    public IActionResult GetCollectionAgendaResult(string refId, string instanceId)
    {
      if (string.IsNullOrEmpty(refId))
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
      var result = _minutzAgendaService.GetMeetingAgendaCollection(Guid.Parse(refId), AuthUser.InfoResponse, schema);
      return result.Condition ? Ok(result.AgendaCollection) : StatusCode(result.Code, result.Message);
    }


    [Authorize]
    [HttpPut("api/feature/agenda/quick", Name = "Quick create agenda")]
    public IActionResult QuickCreateAgendaResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.QuickCreate(request.ReferenceId.ToString(), request.AgendaHeading, request.Order, AuthUser.InfoResponse, schema);
      return result.Condition ? Ok(result.Agenda) : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/complete", Name = "Update agenda complete status")]
    public IActionResult UpdateCompleteResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var userProfile = AuthUser.InfoResponse;//
      var schema =  userProfile.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateComplete(request.Id, request.IsComplete, userProfile, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/order", Name = "Update agenda order")]
    public IActionResult UpdateOrderResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateOrder(request.Id, request.Order, AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/duration", Name = "Update agenda duration")]
    public IActionResult UpdateDurationResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateDuration(request.Id, Convert.ToInt32(request.Duration), AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/title", Name = "Update agenda title")]
    public IActionResult UpdateTitleResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateTitle(request.Id, request.AgendaHeading, AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/text", Name = "Update agenda text")]
    public IActionResult UpdateTextResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateText(request.Id, request.AgendaText, AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/agenda/attendee", Name = "Update attendee")]
    public IActionResult UpdateAssignedAttendeeResult([FromBody] MeetingAgenda request)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
      var result = _minutzAgendaService.UpdateAssignedAttendee(request.Id, request.MeetingAttendeeId, AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpDelete("api/feature/agenda", Name = "Delete agenda")]
    public IActionResult DeleteAgendaResult(Guid id, string instanceId)
    {
      if (!ModelState.IsValid)
        return StatusCode(401, "Request is missing values for the request");
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
      var result = _minutzAgendaService.Delete(id, AuthUser.InfoResponse, schema);
      return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
    }
  }
}