using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Decision;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.ViewModel;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Decision
{
    
    public class MinutzDecisionController : BaseController
    {
        private readonly IMinutzDecisionService _minutzDecisionService;
        
        public MinutzDecisionController(IMinutzDecisionService minutzDecisionService)
        {
            _minutzDecisionService = minutzDecisionService;
        }
        
        [Authorize]
        [HttpGet("api/feature/decisions", Name = "Get the decisions for a meeting")]
        public IActionResult GetMeetingDecisionsResult(string meetingId, string instanceId)
        {
            if (string.IsNullOrEmpty(meetingId))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzDecisionService.GetMeetingDecisions
                (Guid.Parse(meetingId) ,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.DecisionCollection) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPut("api/feature/decision/quick", Name = "Quick create decision")]
        public IActionResult QuickCreateDecisionResult([FromBody] QuickDecisionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzDecisionService.QuickDecisionCreate
                (request.MeetingId, request.DecisionText,request.Order , AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Decision) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/decision/update", Name = "Update decision for a meeting")]
        public IActionResult UpdateDecisionResult([FromBody] MinutzDecision request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzDecisionService.UpdateDecision
                (request.Id, request ,AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Decision) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpDelete("api/feature/decision", Name = "Delete decision for a meeting")]
        public IActionResult DeleteDecisionResult(string decisionId, string meetingId, string instanceId)
        {
            if (string.IsNullOrEmpty(decisionId))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzDecisionService.DeleteDecision
                (Guid.Parse(decisionId), AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
    }
}