using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.Controllers.Feature.Dashboard.Meeting
{
    public class CreateMeetingController : BaseController
    {
        private readonly IUserMeetingsService _userMeetingService;
        public CreateMeetingController(IUserMeetingsService userMeetingService)
        {
            _userMeetingService = userMeetingService;
        }

        [Authorize]
        [HttpPut("api/feature/dashboard/createusermeeting", Name = "Create a User Meeting")]
        public IActionResult UserMeetingsResult()
        {
            var result = _userMeetingService.CreateEmptyUserMeeting(AuthUser.InfoResponse);
            if (result.Condition)
            {
                return Ok(result.Meeting);
            }
            return StatusCode(result.Code, result.Message);
        }
    }
}