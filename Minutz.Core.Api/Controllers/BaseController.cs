using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Controllers
{
    public class BaseController : Controller
    {
        public AuthRestModelResponse AuthUser { get; set; }
    }
}