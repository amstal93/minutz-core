using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Feature;
using Minutz.Core.Api.Models.SignalR;
using Minutz.Core.Feature.UserAdmin;
using Newtonsoft.Json;

namespace Minutz.Core.Api.Controllers {
    [Authorize]
    public class AdminHub : Hub {
        private readonly IUserAdminRepository _userAdminRepository;
        private readonly IConnectionStringService _connectionStringService;

        public AdminHub (
            IUserAdminRepository userAdminRepository,
            IConnectionStringService connectionStringService) {
            _userAdminRepository = userAdminRepository;
            _connectionStringService = connectionStringService;
        }

        public async Task AddToGroup (string groupName) {
            var user = this.Context.ExtractAuth (Context.User);
            await Groups.AddToGroupAsync (Context.ConnectionId, groupName);

            await Clients.Group (groupName)
                .SendAsync ("instanceJoinMessage", $"{user.InfoResponse.Email} has joined the group {groupName}.");
        }

        public async Task RemoveFromGroup (string groupName) {
            var user = this.Context.ExtractAuth (Context.User);
            await Groups.RemoveFromGroupAsync (Context.ConnectionId, groupName);

            await Clients.Group (groupName)
                .SendAsync ("instanceLeaveMessage", $"{user.InfoResponse.Email} has left the group {groupName}.");
        }

        public async Task SendMessage (HubMessage message, string instanceId) {

            var user = Context.ExtractAuth (Context.User);
            var schema = user.InfoResponse.InstanceId;
            message.Stamp = user.InfoResponse.Email;
            if (!string.IsNullOrEmpty (instanceId)) schema = message.InstanceId;
            if (!schema.StartsWith("A_"))
                schema = $"A_{schema.Replace("-", "_")}";
            var masterConnectionString = _connectionStringService.CreateConnectionString ();
            var connection = new SqlConnection (masterConnectionString);
            switch (message.Task.ToAdminTask ()) {
                case AdminTasks.GetUsers:
                    var data = await _userAdminRepository.GetUsersAsync (connection, schema);
                    message = new HubMessage {
                        Data = data,
                        InstanceId = message.InstanceId,
                        Task = message.Task,
                        Stamp = message.Stamp
                    };
                    await Clients.Group (message.InstanceId).SendAsync ("getUsersAsync", message);
                    break;

                case AdminTasks.GetAvailbleUsers:
                    var availbleUsers = _userAdminRepository.GetAvailableUsersAsync (connection, schema);
                    await Clients.Group (message.InstanceId).SendAsync ("getAvailableUsersAsync", message);
                    break;

                case AdminTasks.UpdateUser:
                    InstanceUser updateUser = JsonConvert.DeserializeObject<InstanceUser> (message.Data.user.ToString ());
                    var updateUserData = await _userAdminRepository.UpdateUserAsync (connection, updateUser, schema);
                    message = new HubMessage {
                        Data = updateUserData,
                        InstanceId = message.InstanceId,
                        Task = message.Task,
                        Stamp = message.Stamp
                    };
                    await Clients.Group (message.InstanceId).SendAsync ("updateUserAsync", message);
                    break;
                case AdminTasks.DeleteUser:
                    InstanceUser deleteUser = JsonConvert.DeserializeObject<InstanceUser> (message.Data.user.ToString ());
                    var deleteUserData = await _userAdminRepository.DeleteUserAsync (connection, deleteUser, schema);
                    message = new HubMessage {
                        Data = deleteUserData,
                        InstanceId = message.InstanceId,
                        Task = message.Task,
                        Stamp = message.Stamp
                    };
                    await Clients.Group (message.InstanceId).SendAsync ("deleteUserAsync", message);
                    break;
                case AdminTasks.CreateUser:
                    InstanceUser createUser = JsonConvert.DeserializeObject<InstanceUser> (message.Data.user.ToString ());
                    if (string.IsNullOrEmpty(createUser.FirstName))
                    {
                        var firstNameObject = createUser.Email.Split('@')[0].Replace(".", " ").Substring(0, 1).ToUpper();
                        createUser.FirstName = firstNameObject.Split(' ').Length > 1 
                            ? firstNameObject.Split(' ')[1].Substring(0, 1).ToUpper() : " ";
                    }


                    if (string.IsNullOrEmpty(createUser.LastName))
                    {
                        var lastNameObject = createUser.Email.Split('@')[0].Replace(".", " ").Substring(0, 1).ToUpper();
                        createUser.LastName = lastNameObject.Split(' ').Length > 1 
                            ? lastNameObject.Split(' ')[1].Substring(0, 1).ToUpper() : " ";
                    }

                    if (string.IsNullOrEmpty(createUser.FullName))
                    {
                        createUser.FullName = $"{createUser.FirstName} {createUser.LastName}";
                    }

                    var createUserData = await _userAdminRepository.AddUserAsync (connection, createUser, schema);
                    message = new HubMessage {
                        Data = createUserData,
                        InstanceId = message.InstanceId,
                        Task = message.Task,
                        Stamp = message.Stamp
                    };
                    await Clients.Group (message.InstanceId).SendAsync ("createUserAsync", message);
                    break;
            }
        }
    }
}