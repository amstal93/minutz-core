using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Minutz.Core.Api.Contracts.Date;
using Minutz.Core.Api.Contracts.Decision;
using Minutz.Core.Api.Contracts.Duration;
using Minutz.Core.Api.Contracts.Location;
using Minutz.Core.Api.Contracts.Note;
using Minutz.Core.Api.Contracts.ObjectivePurpose;
using Minutz.Core.Api.Contracts.Recurrence;
using Minutz.Core.Api.Contracts.Tag;
using Minutz.Core.Api.Contracts.Time;
using Minutz.Core.Api.Contracts.Title;
using Minutz.Core.Api.Feature;
using Minutz.Core.Api.Feature.Action;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Feature.MeetingAttendee;
using Minutz.Core.Api.Models.SignalR;
using Minutz.Core.Feature.Agenda;
using Minutz.Core.Api.Feature.Invitation;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;
using Minutz.Core.Api.Models.ViewModel;
using Minutz.Core.Api.Notification;
using Minutz.Core.Api.User;
using Serilog.Core;

namespace Minutz.Core.Api.Controllers 
{
    [Authorize]
    public class MessageHub : Hub
    {
        private readonly IUserManageMeetingService _userManageMeetingService;
        private readonly IUserActionsService _userActionsService;
        private readonly IUserMeetingsService _userMeetingService;
        private readonly IUserMeetingsService _userMeetingsService;
        private readonly IMinutzAgendaService _minutzAgendaService;
        private readonly IMinutzActionService _minutzActionService;
        private readonly IMinutzDecisionService _minutzDecisionService;
        private readonly IMinutzTitleService _minutzTitleService;
        private readonly IMinutzTimeService _minutzTimeService;
        private readonly IMinutzTagService _minutzTagService;
        private readonly IMinutzRecurrenceService _minutzRecurrenceService;
        private readonly IMinutzDateService _minutzDateService;
        private readonly IMinutzLocationService _minutzLocationService;
        private readonly IMinutzNoteService _minutzNoteService;
        private readonly IMeetingObjectivePurposeService _meetingObjectivePurposeService;
        private readonly IMinutzDurationService _minutzDurationService;
        private readonly IMinutzAvailabilityService _minutzAvailabilityService;
        private readonly IMinutzAttendeeService _minutzAttendeeService;
        private readonly IGetMeetingService _getMeetingService;
        private readonly IInvitationService _invitationService;
        private readonly INotificationService _notificationService;
        private readonly IUserRepository _userRepository;

        public MessageHub(
            IUserManageMeetingService userManageMeetingService,
            IUserMeetingsService userMeetingsService,
            IMinutzAgendaService minutzAgendaService,
            IMinutzActionService minutzActionService,
            IMinutzTitleService minutzTitleService,
            IMinutzTimeService minutzTimeService,
            IMinutzTagService minutzTagService,
            IMinutzRecurrenceService minutzRecurrenceService,
            IMinutzDateService minutzDateService,
            IMinutzLocationService minutzLocationService,
            IMinutzNoteService minutzNoteService,
            IMinutzDecisionService minutzDecisionService,
            IMeetingObjectivePurposeService meetingObjectivePurposeService,
            IMinutzDurationService minutzDurationService,
            IMinutzAvailabilityService minutzAvailabilityService,
            IMinutzAttendeeService minutzAttendeeService,
            IGetMeetingService getMeetingService,
            IInvitationService invitationService,
            INotificationService notificationService,
            IUserRepository userRepository,
            IUserActionsService userActionsService)
        {
            _userManageMeetingService = userManageMeetingService;
            _userMeetingsService = userMeetingsService;
            _minutzAgendaService = minutzAgendaService;
            _minutzActionService = minutzActionService;
            _minutzTitleService = minutzTitleService;
            _minutzTimeService = minutzTimeService;
            _minutzTagService = minutzTagService;
            _minutzRecurrenceService = minutzRecurrenceService;
            _minutzDateService = minutzDateService;
            _minutzLocationService = minutzLocationService;
            _minutzNoteService = minutzNoteService;
            _minutzDecisionService = minutzDecisionService;
            _meetingObjectivePurposeService = meetingObjectivePurposeService;
            _minutzDurationService = minutzDurationService;
            _minutzAvailabilityService = minutzAvailabilityService;
            _minutzAttendeeService = minutzAttendeeService;
            _getMeetingService = getMeetingService;
            _invitationService = invitationService;
            _notificationService = notificationService;
            _userRepository = userRepository;
            _userMeetingsService = userMeetingsService;
            _userActionsService = userActionsService;
        }
        
        public async Task AddToGroup(string groupName)
        {
            var user = this.Context.ExtractAuth(Context.User);
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

            await Clients.Group(groupName)
                .SendAsync("InstanceJoinMessage", $"{user.InfoResponse.Email} has joined the group {groupName}.");
        }

        public async Task RemoveFromGroup(string groupName)
        {
            var user = this.Context.ExtractAuth(Context.User);
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

            await Clients.Group(groupName)
                .SendAsync("InstanceLeaveMessage", $"{user.InfoResponse.Email} has left the group {groupName}.");
        }
        
        public async Task SendMessage (HubMessage message, string instanceId)
        {
            var user = Context.ExtractAuth(Context.User);
            var schema = user.InfoResponse.InstanceId;
            message.Stamp = user.InfoResponse.Email;
            if (!string.IsNullOrEmpty(instanceId)) schema = message.InstanceId;
            switch (message.Task.ToTaskType())
            {
                #region Get Methods
                case TaskTypes.getmeetings:
                    var responseMeetings = _userMeetingsService.Meetings(user.InfoResponse);
                    var orderedMeetings = responseMeetings.Meetings.OrderByDescending(i => i.Date).ToList();
                    message.Data = responseMeetings;
                    message.Data.Meetings = orderedMeetings;
                    await Clients.Group(instanceId).SendAsync("MeetingsMessage", message);
                    break;
                case TaskTypes.getmeeting:
                    message.Data =
                        _userMeetingsService.Meeting(user.InfoResponse, Guid.Parse(message.MeetingId),
                            schema);
                    await Clients.Group(message.MeetingId).SendAsync("MeetingMessage", message);
                    break;
                case TaskTypes.getMeetingCollectionAgenda:
                    message.Data =
                        _minutzAgendaService.GetMeetingAgendaCollection(Guid.Parse(message.MeetingId), user.InfoResponse,
                            schema);
                    await Clients.Group(message.MeetingId).SendAsync("MeetingAgendaCollectionMessage", message);
                    break;
                case TaskTypes.getMeetingCollectionActions:
                    message.Data =
                        _minutzActionService.GetMeetingActions(Guid.Parse(message.MeetingId), user.InfoResponse,
                            schema);
                    await Clients.Group(message.MeetingId).SendAsync("MeetingActionCollectionMessage", message);
                    break;
                case TaskTypes.getMeetingCollectionNote:
                    message.Data = _minutzNoteService.GetMeetingNotes(Guid.Parse( message.MeetingId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingNoteCollectionMessageReceived", message);
                    break;
                case TaskTypes.getMeetingCollectionDecision:
                    message.Data = _minutzDecisionService.GetMeetingDecisions(Guid.Parse( message.MeetingId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingDecisionCollectionMessage", message);
                    break;
                case TaskTypes.getActionCollection:
                    var actionsResponse = _userActionsService.Actions(user.InfoResponse);
                    var orderedActions = actionsResponse.Actions.OrderByDescending(i => i.CreatedDate).ToList();
                    message.Data = actionsResponse;
                    message.Data.Actions = orderedActions;
                    await Clients.Group(instanceId).SendAsync("meetingActionCollectionMessageReceived", message);
                    break;
                #endregion
                /////////////////////////////////////// MODE
                #region Mode
                case TaskTypes.setMode:
                   
                    var meetingModel = JsonConvert.DeserializeObject<Meeting>(message.Data.meeting.ToString());
                    MeetingMessage updateResult = _userManageMeetingService.UpdateMeeting(meetingModel, user.InfoResponse, schema);
                    message.Data = new
                    {
                        condition = updateResult.Condition,
                        meeting = updateResult.Meeting,
                        message = updateResult.Message
                    };
                    await Clients.Group(message.InstanceId).SendAsync("updateMeetingMessage", message);
                    break;
                #endregion
                /////////////////////////////////////// MEETING
                #region Meeting Methods
                case TaskTypes.createMeeting:
                    message.Data = _userMeetingsService.CreateEmptyUserMeeting(user.InfoResponse);
                    await Clients.Group(message.InstanceId).SendAsync("createMeetingMessage", message);
                    break;
                case TaskTypes.meetingNameChange:
                    string title = message.Data.title.ToString();
                    message.Data = _minutzTitleService.Update(message.MeetingId, title, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingNameChangeMessage", message);
                    break;
                case TaskTypes.meetingDateChange:
                    string date = message.Data.date.ToString();
                    message.Data = _minutzDateService.Update(message.MeetingId, DateTime.Parse(date), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingDateChangeMessage", message);
                    break;
                case TaskTypes.meetingLocationChange:
                    string location = message.Data.location.ToString();
                    message.Data = _minutzLocationService.Update(message.MeetingId, location, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingLocationChangeMessage", message);
                    break;
                case TaskTypes.meetingTagChange:
                    string tags = message.Data.tags.ToString();
                    message.Data = _minutzTagService.Update(message.MeetingId, tags, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingTagsChangeMessage", message);
                    break;
                case TaskTypes.meetingObjectiveChange:
                    string objective = message.Data.purpose.ToString();
                    message.Data = _meetingObjectivePurposeService.UpdatePurpose
                        (new Guid(message.MeetingId), objective, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingObjectiveChangeMessage", message);
                    break;
                case TaskTypes.meetingOutcomeChange:
                    string outcome = message.Data.outcome.ToString();
                    message.Data = _meetingObjectivePurposeService.UpdateOutcome
                        (new Guid(message.MeetingId), outcome, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingOutcomeChangeMessage", message);
                    break;
                case TaskTypes.meetingDurationChange:
                    string duration = message.Data.duration.ToString();
                    message.Data = _minutzDurationService.Update
                        (message.MeetingId, Int16.Parse(duration), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingDurationChangeMessage", message);
                    break;
                case TaskTypes.meetingTimeChange:
                    string time = message.Data.time.ToString();
                    message.Data = _minutzTimeService.Update
                        (message.MeetingId, time, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingTimeChangeMessage", message);
                    break;

                #endregion
                ///////////////////////////////// ATTENDEES
                #region AttendeesassignNoteUpdate
                case TaskTypes.addMeetingAttendeeInvite:
                    #region Invite
                    Models.Entities.MeetingAttendee invited = JsonConvert.DeserializeObject<Models.Entities.MeetingAttendee>(message.Data.attendee.ToString());
                    var meeting = _getMeetingService.GetMeeting(user.InfoResponse.InstanceId, Guid.Parse(message.MeetingId));
                    
                    var beforeAttendees = _minutzAttendeeService.GetAttendees(Guid.Parse(message.MeetingId), user.InfoResponse,
                        message.InstanceId);
                    var exists = beforeAttendees.Attendees.FirstOrDefault(i => i.Email == invited.Email);
                    if (exists == null)
                    {
                        var inviteRecords = _invitationService.InviteUser(user.InfoResponse, invited, meeting.Meeting);

                        if (inviteRecords.Condition)
                        {
                            var availableAttendees = _minutzAttendeeService.GetAttendees(Guid.Parse(message.MeetingId), user.InfoResponse,
                                message.InstanceId);
                            var newAttendee = availableAttendees.Attendees.FirstOrDefault(i => i.Email == invited.Email);
                            if (newAttendee != null)
                            {
                                if (string.IsNullOrEmpty(newAttendee.Picture)) newAttendee.Picture = "default";
                                if( string.IsNullOrEmpty(newAttendee.InstanceId)) newAttendee.InstanceId = message.InstanceId;
                                newAttendee.ReferenceId = Guid.Parse(message.MeetingId);
                                message.Data = newAttendee;
                            }
                        }
                    }
                    else
                    {
                        message.Data = exists;
                    }
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingInviteAttendeeMessage", message);
                    break;
                    #endregion
                case TaskTypes.addMeetingAttendee:
                    var attendee = JsonConvert.DeserializeObject<Models.Entities.MeetingAttendee>(message.Data.attendee.ToString());
                    message.Data = _minutzAttendeeService.AddAttendee(Guid.Parse( message.MeetingId), attendee, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingAttendeeMessage", message);
                    break;
                case TaskTypes.getMeetingAttendeeCollection:
                    message.Data = _minutzAttendeeService.GetAttendees(Guid.Parse(message.MeetingId),user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingattendeeCollectionMessage", message);
                    break;
                case TaskTypes.getMeetingAvailableCollection:
                    message.Data = _minutzAvailabilityService.GetAvailableAttendees(user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingAvailableCollectionMessage", message);
                    break;
                case TaskTypes.deleteMeetingAttendee:
                    string email = message.Data.email.ToString();
                    message.Data = _minutzAttendeeService.DeleteAttendee(Guid.Parse(message.MeetingId), email, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingattendeeDeleteMessage", message);
                    break;
                case TaskTypes.updateMeetingAttendee:
                    var updateAttendee = JsonConvert.DeserializeObject<Models.Entities.MeetingAttendee>(message.Data.attendee.ToString());
                    message.Data = _minutzAttendeeService.UpdateAttendee(Guid.Parse(message.MeetingId), updateAttendee, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("meetingAttendeeUpdateMessage", message);
                    break;
                #endregion
                ///////////////////////////////// ACTION
                #region Action
                case TaskTypes.addMeetingAction:
                    var actionTitle = message.Data.actionTitle.ToString();
                    var actionOrder = Int32.Parse(message.Data.order.ToString());
                    message.Data = _minutzActionService.QuickCreate(Guid.Parse(message.MeetingId), actionTitle, actionOrder, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingActionMessage", message);
                    break;
                case TaskTypes.deleteMeetingAction:
                    var actionId = message.Data.actionId.ToString();
                    message.Data = _minutzActionService.Delete(Guid.Parse(actionId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("deleteMeetingActionMessage", message);
                    break;
                case TaskTypes.assignAttendeeAction:
                    var assignActionAttendeeEmail =  message.Data.email.ToString();
                    var assignActionActionId = Guid.Parse(message.Data.actionId.ToString());
                    message.Data = _minutzActionService.UpdateActionAssignedAttendee(assignActionActionId, assignActionAttendeeEmail, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignAttendeeActionMessage", message);                    
                    break;
                case TaskTypes.assignDueAction:
                    DateTime assignActionDue = DateTime.Parse(message.Data.due.ToString());
                    var assignActionDueActionId = Guid.Parse(message.Data.actionId.ToString());
                    message.Data = _minutzActionService.UpdateActionDueDate(assignActionDueActionId, assignActionDue, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignDueActionMessage", message);                    
                    break;
                case TaskTypes.assignCreatedAction:
                    DateTime assignActionCreated = DateTime.Parse(message.Data.created.ToString());
                    var assignActionCreatedActionId = Guid.Parse(message.Data.actionId.ToString());
                    message.Data = _minutzActionService.UpdateActionRaisedDate(assignActionCreatedActionId, assignActionCreated, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignCreatedActionMessage", message);                    
                    break;
                case TaskTypes.assignTextAction:
                    string assignActionText =  message.Data.text.ToString();
                    var assignActionTextActionId = Guid.Parse(message.Data.actionId.ToString());
                    message.Data = _minutzActionService.UpdateActionText(assignActionTextActionId, assignActionText, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignTextActionMessage", message);                    
                    break;
                case TaskTypes.assignTitleAction:
                    string assignActionTitle =  message.Data.title.ToString();
                    var assignActionTitleActionId = Guid.Parse(message.Data.actionId.ToString());
                    message.Data = _minutzActionService.UpdateActionTitle(assignActionTitleActionId, assignActionTitle, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignTitleActionMessage", message);                    
                    break;
                case TaskTypes.assignStatusAction:
                    var assignActionIsCompleteValue =  message.Data.isComplete.ToString();
                    bool assignActionIsComplete = false;
                    if (assignActionIsCompleteValue.ToLower() == "true")
                    {
                        assignActionIsComplete = true;
                    }
                    var assignActionStatusActionId = Guid.Parse(message.Data.actionId.ToString());
                    MessageBase response = _minutzActionService.UpdateActionComplete(assignActionStatusActionId, assignActionIsComplete, user.InfoResponse, schema);
                    message.Data = new { Result = response.Message, response.Condition, ActionId = assignActionStatusActionId.ToString()};
                    await Clients.Group(message.MeetingId).SendAsync("assignStatusActionMessage", message);                    
                    break;
                #endregion
                ///////////////////////////////// AGENDA
                #region Agenda
                case TaskTypes.addMeetingAgenda:
                    var agenda = JsonConvert.DeserializeObject<Minutz.Core.Api.Feature.Meeting.MeetingAgenda>(message.Data.agenda.ToString());
                    message.Data = _minutzAgendaService.QuickCreate(message.MeetingId, agenda.AgendaHeading, agenda.Order, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingAgendaMessage", message);
                    break;
                case TaskTypes.deleteMeetingAgenda:
                    var agendaId = message.Data.agendaId.ToString();
                    message.Data = _minutzAgendaService.Delete(Guid.Parse(message.MeetingId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("deleteMeetingAgendaMessage", message);
                    break;
                case TaskTypes.assignOrderAgenda:
                    var assignOrderOrderNUmber =  message.Data.order.ToString();
                    var assignOrderAgendaId = Guid.Parse(message.Data.agendaId.ToString());
                    MessageBase result = _minutzAgendaService.UpdateOrder(assignOrderAgendaId, assignOrderOrderNUmber,
                        user.InfoResponse, schema);
                    message.Data = new
                    {
                        agendaId = assignOrderAgendaId.ToString(),
                        order = result.Message,
                        condition = result.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignOrderAgendaMessage", message);                    
                    break;
                case TaskTypes.assignDurationAgenda:
                    var assignDurationAgenda = Convert.ToInt32(message.Data.duration.ToString());
                    var assignDurationAgendaId = Guid.Parse(message.Data.agendaId.ToString());
                    MessageBase resultDuration = _minutzAgendaService.UpdateDuration(assignDurationAgendaId, assignDurationAgenda, user.InfoResponse, schema);
                    message.Data = new
                    {
                        agendaId = assignDurationAgendaId.ToString(),
                        duration = resultDuration.Message,
                        condition = resultDuration.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignDurationAgendaMessage", message);                    
                    break;
                case TaskTypes.assignTitleAgenda:
                    var assignTitleAgenda = message.Data.AgendaHeading.ToString();
                    var assignTitleAgendaId = Guid.Parse(message.Data.agendaId.ToString());
                    MessageBase AgendaHeadingResult = _minutzAgendaService.UpdateTitle(assignTitleAgendaId, assignTitleAgenda, user.InfoResponse, schema);
                    message.Data = new
                    {
                        agendaId = assignTitleAgendaId.ToString(),
                        AgendaHeading = AgendaHeadingResult.Message,
                        condition = AgendaHeadingResult.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignTitleAgendaMessage", message);                    
                    break;
                case TaskTypes.assignTextAgenda:
                    var assignTextAgenda = message.Data.AgendaText.ToString();
                    var assignTextAgendaId = Guid.Parse(message.Data.agendaId.ToString());
                    MessageBase AgendaTextResult = _minutzAgendaService.UpdateText(assignTextAgendaId, assignTextAgenda, user.InfoResponse, schema);
                    message.Data = new
                    {
                        agendaId = assignTextAgendaId.ToString(),
                        AgendaText = AgendaTextResult.Message,
                        condition = AgendaTextResult.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignTextAgendaMessage", message);                    
                    break;
                case TaskTypes.assignAttendeeAgenda:
                    var assignAttendeeAgenda = message.Data.MeetingAttendeeId.ToString();
                    var assignAttendeeAgendaId = Guid.Parse(message.Data.agendaId.ToString());
                    MessageBase assignAttendeeResult = _minutzAgendaService.UpdateAssignedAttendee(assignAttendeeAgendaId, assignAttendeeAgenda, user.InfoResponse, schema);
                    message.Data = new
                    {
                        agendaId = assignAttendeeAgendaId.ToString(),
                        MeetingAttendeeId = assignAttendeeResult.Message,
                        condition = assignAttendeeResult.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignAttendeeAgendaMessage", message);                    
                    break;
                #endregion
                ///////////////////////////////// NOTE
                #region Note
                case TaskTypes.addMeetingNote:
                    QuickNoteRequest quickNote = JsonConvert.DeserializeObject<QuickNoteRequest>(message.Data.note.ToString());
                    message.Data = _minutzNoteService.QuickNoteCreate(Guid.Parse(message.MeetingId), quickNote.NoteText,quickNote.Order, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingNoteMessage", message);
                    break;
                case TaskTypes.deleteMeetingNote:
                    var noteId = message.Data.noteId.ToString();
                    message.Data = _minutzNoteService.DeleteNote(Guid.Parse(noteId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("deleteMeetingNoteMessage", message);
                    break;
                case TaskTypes.assignNoteUpdate:
                    MeetingNote updateNote =  JsonConvert.DeserializeObject<MeetingNote>(message.Data.note.ToString());
                    message.Data = _minutzNoteService.UpdateNote(Guid.Parse(message.MeetingId), updateNote, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("assignNoteUpdateMessage", message);                    
                    break;
                #endregion
                ///////////////////////////////// DECISION
                #region Decision
                case TaskTypes.addMeetingDecision:
                    QuickDecisionRequest quickDecision = JsonConvert.DeserializeObject<QuickDecisionRequest>(message.Data.decision.ToString());
                    message.Data = _minutzDecisionService.QuickDecisionCreate(Guid.Parse(message.MeetingId), quickDecision.DecisionText,quickDecision.Order, user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("addMeetingDecisionMessage", message);
                    break;
                case TaskTypes.deleteMeetingDecision:
                    var decisionId = message.Data.decisionId.ToString();
                    message.Data = _minutzDecisionService.DeleteDecision(Guid.Parse(decisionId), user.InfoResponse, schema);
                    await Clients.Group(message.MeetingId).SendAsync("deleteMeetingDecisionMessage", message);
                    break;
                case TaskTypes.assignDecisionUpdate:
                    MinutzDecision updateDecision =  JsonConvert.DeserializeObject<MinutzDecision>(message.Data.decision.ToString());
                    DecisionMessage decisionResult = _minutzDecisionService.UpdateDecision(Guid.Parse(message.MeetingId), updateDecision, user.InfoResponse, schema);
                    message.Data = new
                    {
                        decisionId = updateDecision.Id.ToString(),
                        decision = decisionResult.Decision,
                        condition = decisionResult.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("assignDecisionUpdateMessage", message);
                    break;
                #endregion
                #region Person
                case TaskTypes.updatePerson:
                    MeetingAttendee updatePerson =  JsonConvert.DeserializeObject<MeetingAttendee>(message.Data.person.ToString());
                    AttendeeMessage attendeeMessage = _minutzAttendeeService.UpdateAttendee(Guid.Parse(message.MeetingId), updatePerson,user.InfoResponse, schema);
                    message.Data = new
                    {
                        person = attendeeMessage.Attendee,
                        message = attendeeMessage.Message,
                        condition = attendeeMessage.Condition
                    };
                    await Clients.Group(message.MeetingId).SendAsync("updatePersonMessage", message);
                    break;
                #endregion
                default:
                    await Clients.Group(instanceId).SendAsync("ReceiveMessage", message);
                    break;
            }
            
           // await Clients.Group(instanceId).SendAsync("ReceiveMessage", message); meetingTagsChangeMessage
        }
    }
}