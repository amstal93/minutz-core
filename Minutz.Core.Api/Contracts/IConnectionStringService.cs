namespace Minutz.Core.Api.Contracts
{
    public interface IConnectionStringService
    {
        string ClientSecret { get; }
        
        string AuthorityDomain { get; }
        
        string CreateConnectionString(string username, string password);

        string CreateMasterConnectionString();

        string AuthConnectionString();

        string CreateConnectionString();

        string GetInstancePassword(string instance);
    }
}