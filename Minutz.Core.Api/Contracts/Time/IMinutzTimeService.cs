using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Time
{
    public interface IMinutzTimeService
    {
        MessageBase Update(string meetingId, string time, AuthRestModel user, string schema);
    }
}