using System;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Note
{
    public interface IMinutzNoteService
    {
        NoteMessage GetMeetingNotes(Guid meetingId, AuthRestModel user, string schema);

        NoteMessage QuickNoteCreate(Guid meetingId, string noteText, int order, AuthRestModel user, string schema);

        NoteMessage UpdateNote(Guid meetingId, MeetingNote note, AuthRestModel user, string schema);

        MessageBase DeleteNote(Guid noteId, AuthRestModel user, string schema);
    }
}