using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Tag
{
    public interface IMinutzTagRepository
    {
        MessageBase Update(string meetingId, string tags, string schema, string connectionString);
    }
}