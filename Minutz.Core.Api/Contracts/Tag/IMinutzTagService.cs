using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Tag
{
    public interface IMinutzTagService
    {
        MessageBase Update(string meetingId, string tags, AuthRestModel user, string schema);
    }
}