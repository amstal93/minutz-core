using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Title
{
    public interface IMinutzTitleRepository
    {
        MessageBase Update(string meetingId, string title, string schema, string connectionString);
    }
}