using System.Threading.Tasks;

namespace Minutz.Core.Api.Contracts.Hubs
{
    public interface IChatClient
    {
        Task Send(string message);
    }
}