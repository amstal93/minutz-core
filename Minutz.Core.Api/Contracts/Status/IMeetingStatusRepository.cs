using System;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Status
{
    public interface IMeetingStatusRepository
    {
        MessageBase UpdateMeetingStatus(Guid meetingId, string status, string schema, string connectionString);
    }
}