using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Location
{
    public interface IMinutzLocationRepository
    {
        MessageBase Update(string meetingId, string location, string schema, string connectionString);
    }
}