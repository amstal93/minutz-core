using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Duration
{
    public interface IMinutzDurationService
    {
        MessageBase Update(string meetingId, int duration, AuthRestModel user, string schema);
    }
}