using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Recurrence
{
    public interface IMinutzRecurrenceService
    {
        MessageBase Update(string meetingId, int recurrence, AuthRestModel user, string schema);
    }
}