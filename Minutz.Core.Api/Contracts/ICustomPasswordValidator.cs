using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Contracts
{
    public interface ICustomPasswordValidator
    {
        PasswordScore CheckStrength(string password);
    }
}