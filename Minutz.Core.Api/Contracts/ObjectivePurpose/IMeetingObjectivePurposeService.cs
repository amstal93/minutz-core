using System;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Contracts.ObjectivePurpose
{
    public interface IMeetingObjectivePurposeService
    {
        MeetingMessage UpdateOutcome(Guid meetingId, string objective, AuthRestModel user, string schema);

        MeetingMessage UpdatePurpose(Guid meetingId, string purpose, AuthRestModel user, string schema);
    }
}