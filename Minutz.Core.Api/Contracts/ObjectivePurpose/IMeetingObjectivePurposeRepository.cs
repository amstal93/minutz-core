using System;
using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.Contracts.ObjectivePurpose
{
    public interface IMeetingObjectivePurposeRepository
    {
        MeetingMessage UpdateOutcome(Guid meetingId, string objective, string schema, string connectionString);

        MeetingMessage UpdatePurpose(Guid meetingId, string purpose, string schema, string connectionString);
    }
}