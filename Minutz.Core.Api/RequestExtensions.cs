using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api {
    public static class RequestExtensions {
        public static AuthRestModelResponse ExtractAuth (this HttpRequest request, ClaimsPrincipal user) {
            var email = user.Claims.ToList ().SingleOrDefault (i => i.Type == "access_token");
            if (email == null) throw new Exception ("The user Identity does not contain a sub [email].");
            var instanceId = user.Claims.ToList ().SingleOrDefault (i => i.Type == "instanceId");
            if (instanceId == null) throw new Exception ("The user Identity does not contain instanceId.");
            var roles = user.Claims.ToList ().SingleOrDefault (i => i.Type == "roles");
            var role = user.Claims.ToList ().SingleOrDefault (i => i.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");
            if (roles == null) {
                if (role != null) {
                    roles = role;
                }
                else {
                    throw new Exception ("The user Identity does not contain role.");
                }
            }

            var name = user.Claims.ToList ().SingleOrDefault (i => i.Type == "nickname");
            if (name == null) throw new Exception ("The user Identity does not contain name.");
            var picture = user.Claims.ToList ().SingleOrDefault (i => i.Type == "picture");
            if (picture == null) throw new Exception ("The user Identity does not contain picture.");
            var exp = user.Claims.ToList ().SingleOrDefault (i => i.Type == "exp");
            if (exp == null) throw new Exception ("The user Identity does not contain exp.");
            var accessToken = user.Claims.ToList ().SingleOrDefault (i => i.Type == "access_token");
            if (accessToken == null) throw new Exception ("The user Identity does not contain exp.");
            var result = new AuthRestModel {
                Email = email.Value,
                Name = name.Value,
                Picture = picture.Value,
                Nickname = name.Value,
                Role = roles.Value,
                InstanceId = instanceId.Value,
                TokenExpire = exp.Value,
                Sub = email.Value,
                AccessToken = accessToken.Value
            };
            return new AuthRestModelResponse {
                Condition = true,
                    Code = 200,
                    Message = "",
                    InfoResponse = result
            };
        }

        public static AuthRestModelResponse ExtractAuth (this HubCallerContext request, ClaimsPrincipal user) {

            var email = user.Claims.ToList ().SingleOrDefault (i => i.Type == "access_token");
            if (email == null) throw new Exception ("The user Identity does not contain a sub [email].");
            var instanceId = user.Claims.ToList ().SingleOrDefault (i => i.Type == "instanceId");
            if (instanceId == null) throw new Exception ("The user Identity does not contain instanceId.");
            var roles = user.Claims.ToList ().SingleOrDefault (i => i.Type == "roles");
            var role = user.Claims.ToList ().SingleOrDefault (i => i.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");
            if (roles == null) {
                if (role != null) {
                    roles = role;
                }
                else {
                    throw new Exception ("The user Identity does not contain role.");
                }
            }

            var name = user.Claims.ToList ().SingleOrDefault (i => i.Type == "nickname");
            if (name == null) throw new Exception ("The user Identity does not contain name.");
            var picture = user.Claims.ToList ().SingleOrDefault (i => i.Type == "picture");
            if (picture == null) throw new Exception ("The user Identity does not contain picture.");
            var exp = user.Claims.ToList ().SingleOrDefault (i => i.Type == "exp");
            if (exp == null) throw new Exception ("The user Identity does not contain exp.");
            var accessToken = user.Claims.ToList ().SingleOrDefault (i => i.Type == "access_token");
            if (accessToken == null) throw new Exception ("The user Identity does not contain exp.");
            var result = new AuthRestModel {
                Email = email.Value,
                Name = name.Value,
                Picture = picture.Value,
                Nickname = name.Value,
                Role = role.Value,
                InstanceId = instanceId.Value,
                TokenExpire = exp.Value,
                Sub = email.Value,
                AccessToken = accessToken.Value
            };
            return new AuthRestModelResponse {
                Condition = true,
                    Code = 200,
                    Message = "",
                    InfoResponse = result
            };
        }
    }
}