SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [app].[spCreateUserAndSchema] --this is the main script
    @Identity       NVARCHAR(255),
    @Email          NVARCHAR(255),
    @tenant         NVARCHAR(max),
    @EncryptedPasswordString NVARCHAR(MAX),
    @PasswordString NVARCHAR(MAX)
AS

-- DECLARE @tenant NVARCHAR(MAX);
-- SET @tenant = 'foo';

-- DECLARE @PasswordString NVARCHAR(MAX);
-- SET @PasswordString = 'bGRQX5Tqzd';

  INSERT into [app].[Instance] ([Name], [Username], [Password], [Active], [Type])
  VALUES (@Email, @tenant, @EncryptedPasswordString, 1, 1);

  UPDATE [app].[Person]
  SET [InstanceId] = @tenant,
      [Role]       = 'User'
  WHERE Identityid = @Identity

  DECLARE @createloginSQL nvarchar(4000) = 'CREATE LOGIN ' + @tenant + ' WITH PASSWORD = ''' + @PasswordString + ''' ';
  EXEC sys.sp_executesql @createloginSQL

  DECLARE @createloginuserSQL nvarchar(4000) = 'CREATE USER ' + @tenant + ' FOR LOGIN ' + @tenant + ' ';
  EXEC sys.sp_executesql @createloginuserSQL

  DECLARE @createSchemaSQL nvarchar(1000) = 'CREATE SCHEMA ' + @tenant + ' ';
  EXEC sp_executesql @createSchemaSQL

  DECLARE @assigPermissionsSchemaSQL nvarchar(4000) =
  'GRANT SELECT, UPDATE, DELETE, INSERT ON SCHEMA :: ' + @tenant + ' TO ' + @tenant + ' ';
  EXEC sp_executesql @assigPermissionsSchemaSQL

  DECLARE @createMeetingTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[Meeting] (
[Id] uniqueidentifier NOT NULL,
[Name] VARCHAR (MAX) NOT NULL,
[Location] VARCHAR (255) NULL,
[Date] DATETIME2 NULL,
[UpdatedDate] DATETIME2 NULL,
[Time] VARCHAR (255) NULL,
[Duration] INT NULL,
[IsRecurrence] BIT NULL,
[IsPrivate] BIT NULL,
[RecurrenceType] VARCHAR (255) NULL,
[IsLocked] BIT NULL,
[IsFormal] BIT NULL,
[TimeZone] VARCHAR (255) NULL,
[Tag] VARCHAR (255) NULL,
[Purpose] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[MeetingOwnerId] VARCHAR (255) NULL,
[Outcome] VARCHAR (255) NULL
)

CREATE TABLE [' + @tenant + '].[Meeting_Audit]
(
[Id] uniqueidentifier NOT NULL,
[Name] VARCHAR (MAX) NOT NULL,
[Location] VARCHAR (255) NULL,
[Date] DATETIME2 NULL,
[UpdatedDate] DATETIME2 NULL,
[Time] VARCHAR (255) NULL,
[Duration] INT NULL,
[IsRecurrence] BIT NULL,
[IsPrivate] BIT NULL,
[RecurrenceType] VARCHAR (255) NULL,
[IsLocked] BIT NULL,
[IsFormal] BIT NULL,
[TimeZone] VARCHAR (255) NULL,
[Tag] VARCHAR (255) NULL,
[Purpose] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[MeetingOwnerId] VARCHAR (255) NULL,
[Outcome] VARCHAR (255) NULL,
[Email] VARCHAR (255) NULL,
[Role] VARCHAR (255) NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingTablesSql

  DECLARE @createMeetingAttendeeTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MeetingAttendee]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[PersonIdentity] VARCHAR (255) NULL,
[Email] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[Role] VARCHAR (255) NULL,
[Order] INT NULL
)

CREATE TABLE [' + @tenant + ' ].[MeetingAttendee_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[PersonIdentity] VARCHAR (255) NULL,
[Email] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[Role] VARCHAR (255) NULL,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingAttendeeTablesSql

  DECLARE @createAvailableAttendeeTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[AvailableAttendee]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[PersonIdentity] VARCHAR (255) NULL,
[Email] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[Role] VARCHAR (255) NULL,
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[AvailableAttendee_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[PersonIdentity] VARCHAR (255) NULL,
[Email] VARCHAR (255) NULL,
[Status] VARCHAR (255) NULL,
[Role] VARCHAR (255) NULL,
[Action] VARCHAR (255) NULL,
[Order] INT NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createAvailableAttendeeTablesSql

  DECLARE @createMeetingAgendaTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MeetingAgenda]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[AgendaHeading] VARCHAR (255) NULL,
[AgendaText] VARCHAR (255) NULL,
[MeetingAttendeeId] VARCHAR (255) NULL,
[Duration] VARCHAR (255) NULL,
[CreatedDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MeetingAgenda_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[AgendaHeading] VARCHAR (255) NULL,
[AgendaText] VARCHAR (255) NULL,
[MeetingAttendeeId] VARCHAR (255) NULL,
[Duration] VARCHAR (255) NULL,
[CreatedDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingAgendaTablesSql

  DECLARE @createMeetingActionTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MeetingAction]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[ActionText] VARCHAR (255) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[DueDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MeetingAction_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[ActionText] VARCHAR (255) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[DueDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingActionTablesSql

  DECLARE @createMinutzActionTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MinutzAction]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[ActionText] VARCHAR (255) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[DueDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MinutzAction_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[ActionText] VARCHAR (255) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[DueDate] DATETIME2 NULL,
[IsComplete] BIT NOT NULL DEFAULT 0,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMinutzActionTablesSql

  DECLARE @createMeetingNoteTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MeetingNote]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[NoteText] VARCHAR (255) NULL,
[MeetingAttendeeId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MeetingNote_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[NoteText] VARCHAR (255) NULL,
[MeetingAttendeeId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingNoteTablesSql

  DECLARE @createMeetingAttachmentTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MeetingAttachment]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[FileName] VARCHAR (MAX) NULL,
[MeetingAttendeeId] uniqueidentifier NULL,
[Date] DATETIME2 NULL,
[FileData] VARBINARY(MAX) NULL,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MeetingAttachment_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[FileName] VARCHAR (MAX) NULL,
[MeetingAttendeeId] uniqueidentifier NULL,
[Date] DATETIME2 NULL,
[FileData] VARBINARY(MAX) NULL,
[Action] VARCHAR (255) NULL,
[Order] INT NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)';
  EXEC sp_executesql @createMeetingAttachmentTablesSql

  DECLARE @createMinutzDecisionTablesSql NVARCHAR(max) = '
CREATE TABLE [' + @tenant + '].[MinutzDecision]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[DecisionText] VARCHAR (max) NULL,
[DecisionComment] VARCHAR (max) NULL,
[AgendaId] VARCHAR (max) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[IsOverturned] BIT NULL,
[Identifier] INT NOT NULL IDENTITY(1,1),
[Order] INT NULL
)

CREATE TABLE [' + @tenant + '].[MinutzDecision_Audit]
(
[Id] uniqueidentifier NOT NULL,
[ReferenceId] uniqueidentifier NOT NULL,
[DecisionText] VARCHAR (max) NULL,
[DecisionComment] VARCHAR (max) NULL,
[AgendaId] VARCHAR (max) NULL,
[PersonId] uniqueidentifier NULL,
[CreatedDate] DATETIME2 NULL,
[IsOverturned] BIT NULL,
[Order] INT NULL,
[Action] VARCHAR (255) NULL,
[ChangedBy] VARCHAR (255) NULL,
[AuditDate] DATETIME2 NOT NULL
)
';
  EXEC sp_executesql @createMinutzDecisionTablesSql
GO
